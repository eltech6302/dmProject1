﻿#include "Q.hpp"

Q RED_Q_Q(Q a)
{
	//получает НОД числителя и знаменателя, выраженный в целых числах
	Z nod = TRANS_N_Z(GCF_NN_N(a.nom.num, a.denom.num));

	//деление числителя на НОД
	a.nom = DIV_ZZ_Z(a.nom, nod);

	//деление знаменателя на НОД
	a.denom = DIV_ZZ_Z(a.denom, nod);

	return a;
}


bool INT_Q_B(Q a)
{
	if (a.denom.num.size() == 1 && a.denom.num[0] == 1)
		return true;
	return false;
}


Q TRANS_Z_Q(Z a)
{
	Q b;
	b.nom = a;
	b.denom.num.push_back(1);

	return b;
}


Z removeZero(Z a)
{
	while (a.num.size() > 1 && a.num.front() == 0)
	{
		a.num.erase(a.num.begin());
	}

	return a;
}


Z TRANS_Q_Z(Q a)
{
	a.denom = removeZero(a.denom);

	if (a.denom.num.size() == 1 && a.denom.num[0] == 1)
	{
		if (!(a.denom.pos))
			a.nom.pos = !(a.nom.pos);

		return a.nom;
	}

	throw "Q-4";
}


Q ADD_QQ_Q(Q a, Q b)
{
	Z t;
	t.pos = true;
	t.num = LCM_NN_N(a.denom.num, b.denom.num);

	a.nom = ADD_ZZ_Z(MUL_ZZ_Z(a.nom, DIV_ZZ_Z(t, a.denom)), MUL_ZZ_Z(b.nom, DIV_ZZ_Z(t, b.denom))); 

	a.denom = t;

	return RED_Q_Q(a);
}


Q SUB_QQ_Q(Q a, Q b)
{
	b.nom.pos = !b.nom.pos;

	a = ADD_QQ_Q(a, b);

	return RED_Q_Q(a);
}


Q MUL_QQ_Q(Q a, Q b)
{
	a.nom = MUL_ZZ_Z(a.nom, b.nom);
	a.denom = MUL_ZZ_Z(a.denom, b.denom);

	return RED_Q_Q(a);
}


Q DIV_QQ_Q(Q a, Q b)
{
	b.nom = removeZero(b.nom);

	if (b.nom.num.size() == 1 && b.nom.num[0] == 0)
		throw "Q-8";

	Z tmp = b.nom;
	b.nom = b.denom;
	b.denom = tmp;

	return MUL_QQ_Q(a, b);
}