#include "Process.hpp"
#include "P.hpp"


N ReadNumber_N(System::String^ str)
{
	N num;
	while (str[0] == ' ')
		str = str->Remove(0, 1);

	for (int i = 0; i < str->Length && str[i] >= '0' && str[i] <= '9'; i++)
		num.push_back((int)str[i] - 48);

	return num;
}

System::String^ WriteNumber_N(N num)
{
	System::String^ str = "";

	for (int i = 0; i < num.size(); i++)
	{
		str = String::Concat(str, num[i].ToString());
	}

	return str;
}

System::String^ Process_N(System::String^ str)
{
	std::vector <N> num_in_str;
	N num;
	char op = ' ';

	int j = 0;
	if (str->Length != 0)
	{
		if (str->StartsWith("���: ") || str->StartsWith("���: ") || str->StartsWith("��������: "))
		{
			bool GCF = str->StartsWith("���: ");
			bool COM;
			str = str->Remove(0, 5);
			if (str->Contains("��"))
			{
				COM = true;
				str = str->Remove(0, 5);
			}

			num = ReadNumber_N(str);
			str = str->Remove(0, num.size());
			num_in_str.push_back(num);

			if (str->Length != 0 && str[0] == ',')
			{
				str = str->Remove(0, 2);
				num_in_str.push_back(ReadNumber_N(str));

				if (str->Length != 0)
					str = str->Remove(0);

				if (GCF)
				{
					try
					{
						num_in_str[0] = GCF_NN_N(num_in_str[0], num_in_str[1]);
					}
					catch (int err_num)
					{
						str = "ERROR N-" + err_num + ". N < 0!";
						return str;
					}
				}
				else if (COM)
				{
					int t = COM_NN_D(num_in_str[0], num_in_str[1]);

					str = String::Concat(str, WriteNumber_N(num_in_str[0]));

					if (t == 2)
						str = String::Concat(str, " > ");
					else if (t == 1)
						str = String::Concat(str, " < ");
					else
						str = String::Concat(str, " = ");

					str = String::Concat(str, WriteNumber_N(num_in_str[1]));

					return str;
				}
				else
					num_in_str[0] = LCM_NN_N(num_in_str[0], num_in_str[1]);
			}
		}
		else
		{
			num_in_str.push_back(ReadNumber_N(str));

			str = str->Remove(0, num_in_str[0].size());

			if (str->Length != 0)
			{
				while (str->Length != 0 && str[0] == ' ')
				{
					str = str->Remove(0, 1);
					if (str->Length != 0)
						op = str[0];
				}

				if (op == 'D' || op == 'M')
					str = str->Remove(0, 2);

				str = str->Remove(0, 2);

				num_in_str.push_back(ReadNumber_N(str));


				switch (op)
				{
				case '+':
					num_in_str[0] = ADD_NN_N(num_in_str[0], num_in_str[1]);
					break;
				case '-':
					try
					{
						num_in_str[0] = SUB_NN_N(num_in_str[0], num_in_str[1]);
					}
					catch (int err_num)
					{
						str = "ERROR N-" + err_num + ". N < 0!";
						return str;
					}
					break;
				case '*':
					num_in_str[0] = MUL_NN_N(num_in_str[0], num_in_str[1]);
					break;
				case 'D':
					try
					{
						num_in_str[0] = DIV_NN_N(num_in_str[0], num_in_str[1]);
						break;
					}
					catch (int err_num)
					{
						str = "ERROR N-" + err_num + ". DIVISION BY 0!";
						return str;
					}
				case 'M':
					try
					{
						num_in_str[0] = MOD_NN_N(num_in_str[0], num_in_str[1]);
						break;
					}
					catch (int err_num)
					{
						str = "ERROR N-" + err_num + ". DIVISION BY 0!";
						return str;
					}
				default:
					num_in_str[0] = { 0, 0, 0, 0 };
				}
			}
		}

		num_in_str[0] = RMVZ_N_N(num_in_str[0]);

		if (num_in_str[0].size() == 0)
			num_in_str[0].push_back(0);

		str = WriteNumber_N(num_in_str[0]);
	}
	return str;
}


Z ReadNumber_Z(System::String^ str)
{
	Z num = { {0}, true };
	
	if (str->Length != 0)
	{
		if (str[0] == '-')
		{
			num.pos = false;
			str = str->Remove(0, 1);
		}

		num.num = ReadNumber_N(str);
	}
	return num;
}

System::String^ WriteNumber_Z(Z num)
{
	System::String^ str = "";
	if (num.pos == false)
		str = String::Concat(str, "-");

	str = String::Concat(str, WriteNumber_N(num.num));

	return str;
}

System::String^ Z_Process(System::String^ str)
{
	std::vector <Z> num_in_str;
	Z num;
	num.pos = true;
	char op = ' ';

	if (str->Length != 0)
	{
		if (str->StartsWith("���: ") || str->StartsWith("���: ") || str->StartsWith("��������: "))
		{
			bool GCF = str->StartsWith("���: ");
			bool COM;
			str = str->Remove(0, 5);
			if (str->Contains("��"))
			{
				COM = true;
				str = str->Remove(0, 5);
			}

			num = ReadNumber_Z(str);
			str = str->Remove(0, num.num.size() + !num.pos);
			num_in_str.push_back(num);

			if (str->Length != 0 && str[0] == ',')
			{
				str = str->Remove(0, 2);
				num_in_str.push_back(ReadNumber_Z(str));

				if (str->Length != 0)
					str = str->Remove(0);

				if (GCF)
				{
					num_in_str[0].num = GCF_NN_N(num_in_str[0].num, num_in_str[1].num);
					num_in_str[0].pos = true;
				}
				else if (COM)
				{
					int t, z; //t �������� �� ��������� �� ������, � z �� ��������� ������
					if (num_in_str[0].pos > num_in_str[1].pos)
					{
						z = 2;
						t = 2;
					}
					else if (num_in_str[0].pos < num_in_str[1].pos)
					{
						z = 1;
						t = 1;
					}
					else
					{
						z = 0;
						t = COM_NN_D(num_in_str[0].num, num_in_str[1].num);
					}

					str = WriteNumber_Z(num_in_str[0]);

					if (z == 2 || (z == 0 && ((num_in_str[0].pos && t == 2) || (num_in_str[0].pos == false && t == 1))))
						str = String::Concat(str, " > ");
					else if (z == 1 || (z == 0 && ((num_in_str[0].pos && t == 1) || (num_in_str[0].pos == false && t == 2))))
						str = String::Concat(str, " < ");
					else
						str = String::Concat(str, " = ");

					str = String::Concat(str, WriteNumber_Z(num_in_str[1]));

					return str;
				}
				else
				{
					num_in_str[0].num = LCM_NN_N(num_in_str[0].num, num_in_str[1].num);
					num_in_str[0].pos = true;
				}
			}
		}
		else
		{
			num_in_str.push_back(ReadNumber_Z(str));
			str = str->Remove(0, num_in_str[0].num.size() + !(num_in_str[0].pos));
			if (str->Length != 0)
			{
				while (str->Length != 0 && str[0] == ' ')
				{
					str = str->Remove(0, 1);
					if (str->Length != 0)
						op = str[0];
				}

				if (op == 'D' || op == 'M')
					str = str->Remove(0, 2);

				str = str->Remove(0, 2);

				num_in_str.push_back(ReadNumber_Z(str));
				str = str->Remove(0);

				switch (op)
				{
				case '+':
					num_in_str[0] = ADD_ZZ_Z(num_in_str[0], num_in_str[1]);
					break;
				case '-':
					num_in_str[0] = SUB_ZZ_Z(num_in_str[0], num_in_str[1]);
					break;
				case '*':
					num_in_str[0] = MUL_ZZ_Z(num_in_str[0], num_in_str[1]);
					break;
				case 'D':
					try
					{
						num_in_str[0] = DIV_ZZ_Z(num_in_str[0], num_in_str[1]);
						break;
					}
					catch (int err_num)
					{
						str = "ERROR Z-" + err_num + ". DIVISION BY 0!";
						return str;
					}
				case 'M':
					try
					{
						num_in_str[0] = MOD_ZZ_Z(num_in_str[0], num_in_str[1]);
						break;
					}
					catch (int err_num)
					{
						str = "ERROR Z-" + err_num + ". DIVISION BY 0!";
						return str;
					}
				default:
					num_in_str[0] = { { 0, 0, 0, 0 }, true };
				}
			}
		}

		num_in_str[0].num = RMVZ_N_N(num_in_str[0].num);

		if (num_in_str[0].num.size() == 0)
			num_in_str[0].num.push_back(0);

		str = String::Concat(str, WriteNumber_Z(num_in_str[0]));
	}
	return str;
}


Q ReadNumber_Q(System::String^ str)
{
	Q num = { {{0}, true}, {{1}, true } };
	if (str->Length != 0)
	{
		num.nom = ReadNumber_Z(str);
		str = str->Remove(0, num.nom.num.size() + !(num.nom.pos));
		if (str->Length != 0 && str[0] == '/')
		{
			str = str->Remove(0, 1);
			num.denom = ReadNumber_Z(str);
		}
		else
		{
			num.denom = { {1}, true };
		}
	}
	return num;
}

System::String^ WriteNumber_Q(Q num)
{
	String^ str = "";
	str = String::Concat(WriteNumber_Z(num.nom));

	if (COM_NN_D(num.denom.num, { 1 }) != 0)
	{
		str = String::Concat(str, "/" + WriteNumber_Z(num.denom));
	}

	return str;
}

System::String^ Q_Process(System::String^ str)
{
	std::vector <Q> num_in_str;
	char op = ' ';
	if (str->Length != 0)
	{
		if (str->StartsWith("��������: "))
		{
			str = str->Remove(0, 10);

			num_in_str.push_back(ReadNumber_Q(str));

			int d = 0;
			if (str->IndexOf("/") < num_in_str[0].nom.num.size() + !num_in_str[0].nom.pos + num_in_str[0].denom.num.size() + !num_in_str[0].denom.pos)
			{
				d = 1;
			}

			str = str->Remove(0, num_in_str[0].nom.num.size() + !num_in_str[0].nom.pos + num_in_str[0].denom.num.size() + !num_in_str[0].denom.pos + d);

			if (str->Length != 0 && str[0] == ',')
			{
				str = str->Remove(0, 2);
				num_in_str.push_back(ReadNumber_Q(str));
				if (str->Length != 0)
					str = str->Remove(0);

				int z /*��������� �� �����*/, t /*��������� �� ���������*/; //��������� ������������ �� ��������� ��� ������ ���������� ������ � ������ �����������
				Q n1 = num_in_str[0], n2 = num_in_str[1];

				N LCM = LCM_NN_N(n1.denom.num, n2.denom.num);
				N GCF1 = GCF_NN_N(n1.denom.num, LCM), GCF2 = GCF_NN_N(n2.denom.num, LCM);

				n1.nom = DIV_ZZ_Z(MUL_ZZ_Z(n1.nom, TRANS_N_Z(LCM)), TRANS_N_Z(GCF1));
				n2.nom = DIV_ZZ_Z(MUL_ZZ_Z(n2.nom, TRANS_N_Z(LCM)), TRANS_N_Z(GCF2));

				if (num_in_str[0].nom.pos > num_in_str[1].nom.pos)
				{
					z = 2;
					t = 2;
				}
				else if (num_in_str[0].nom.pos < num_in_str[1].nom.pos)
				{
					z = 1;
					t = 1;
				}
				else
				{
					z = 0;
					t = COM_NN_D(n1.nom.num, n2.nom.num);
				}

				str = WriteNumber_Q(num_in_str[0]);

				if (z == 2 || (z == 0 && ((num_in_str[0].nom.pos && t == 2) || (num_in_str[0].nom.pos == false && t == 1))))
					str = String::Concat(str, " > ");
				else if (z == 1 || (z == 0 && ((num_in_str[0].nom.pos && t == 1) || (num_in_str[0].nom.pos == false && t == 2))))
					str = String::Concat(str, " < ");
				else
					str = String::Concat(str, " = ");

				str = String::Concat(str, WriteNumber_Q(num_in_str[1]));

				return str;
			}
		}
		else
		{
			num_in_str.push_back(ReadNumber_Q(str));
			int d = 0;
			if (str->IndexOf("/") < num_in_str[0].nom.num.size() + !num_in_str[0].nom.pos + num_in_str[0].denom.num.size() + !num_in_str[0].denom.pos)
			{
				d = 1;
			}

			str = str->Remove(0, num_in_str[0].nom.num.size() + !num_in_str[0].nom.pos + num_in_str[0].denom.num.size() + !num_in_str[0].denom.pos + d);

			if (str->Length != 0)
			{
				while (str->Length != 0 && str[0] == ' ')
				{
					str = str->Remove(0, 1);
					if (str->Length != 0)
						op = str[0];
				}

				if (op == 'D' || op == 'M')
					str = str->Remove(0, 2);

				str = str->Remove(0, 2);

				num_in_str.push_back(ReadNumber_Q(str));

				str = str->Remove(0);

				switch (op)
				{
				case '+':
					num_in_str[0] = ADD_QQ_Q(num_in_str[0], num_in_str[1]);
					break;
				case '-':
					num_in_str[0] = SUB_QQ_Q(num_in_str[0], num_in_str[1]);
					break;
				case '*':
					num_in_str[0] = MUL_QQ_Q(num_in_str[0], num_in_str[1]);
					break;
				default:
					num_in_str[0] = { { {0}, true}, { {1}, true} };
				}
			}

		}


		str = String::Concat(str, WriteNumber_Q(RED_Q_Q(num_in_str[0])));



		if (str == "")
			str = "0";
	}

	return str;
}


P ReadPolinom(System::String^ str)
{
	P pol;
	int j = 0;
	for (int i = 0; i < str->Length && str[0] != ')';)
	{
		if (str[i].IsLetter(str[i]))
		{
			str = str->Remove(0, 1);
			if (str[i] == '^')
			{
				str = str->Remove(0, 1);
				N tmp = pol.deg;
				pol.deg = ReadNumber_N(str);
				str = str->Remove(0, pol.deg.size());
				i = 0;
				if (COM_NN_D(tmp, pol.deg) == 2)
					pol.deg = tmp;
			}
		}
		else if (str[i].IsDigit(str[i]) || str[i] == '-')
		{
			auto it = pol.coef.begin();
			pol.coef.insert(it, ReadNumber_Q(str));
			int d = 0;
			if (str->IndexOf("/") < pol.coef[0].nom.num.size() + !pol.coef[0].nom.pos + pol.coef[0].denom.num.size() + !pol.coef[0].denom.pos)
			{
				d = 1;
				str = str->Remove(0, pol.coef[0].nom.num.size() + !pol.coef[0].nom.pos + pol.coef[0].denom.num.size() + !pol.coef[0].denom.pos + d);
			}
			else
				str = str->Remove(0, pol.coef[0].nom.num.size() + !pol.coef[0].nom.pos);
			i = 0;
			j++;
		}
		else
		{
			str = str->Remove(0, 1);
		}
	}

	return pol;
}

P Process_P(System::String^ str)
{
	std::vector <P> pols;
	char op = ' ';
	while (str->Length != 0)
	{
		if (str[0] == '(')
		{
			pols.push_back(ReadPolinom(str));
			str = str->Remove(0, str->IndexOf(')') + 1);
			if (op != ' ')
			{
				switch (op)
				{
				case '+':
					pols[0] = ADD_PP_P(pols[0], pols[1]);
					break;
				case '-':
					pols[0] = SUB_PP_P(pols[0], pols[1]);
					break;
				case '*':
					pols[0] = MUL_PP_P(pols[0], pols[1]);
					break;
				case '/':
					pols[0] = DIV_PP_P(pols[0], pols[1]);
					break;
				}
			}
		}
		else if (str[0] == '+' || str[0] == '-' || str[0] == '*' || str[0] == '/')
		{
			op = str[0];
			str = str->Remove(0, 1);
		}
		else
			str = str->Remove(0, 1);
	}

	return pols[0];
}

void Output_P(P a)
{
	
}