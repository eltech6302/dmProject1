﻿#pragma once

#include "P.hpp"
#include "Process.hpp"

namespace dmProject1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Сводка для Main
	/// </summary>
	public ref class Main : public System::Windows::Forms::Form
	{
	public:
		Main(void)
		{
			InitializeComponent();
			//
			//TODO: добавьте код конструктора
			//
		}

	protected:
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		~Main()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^  справкаToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  оПрограммеToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  основныеВозможнстиToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  форматВводаToolStripMenuItem;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage1;


	private: System::Windows::Forms::TabPage^  tabPage2;





	private: System::Windows::Forms::TextBox^  NN_output;
	private: System::Windows::Forms::Button^  N_SubOne;

	private: System::Windows::Forms::Button^  N_Zero;
	private: System::Windows::Forms::Button^  N_AddOne;


	private: System::Windows::Forms::Button^  N_Three;

	private: System::Windows::Forms::Button^  N_Two;

	private: System::Windows::Forms::Button^  N_One;
	private: System::Windows::Forms::Button^  N_Six;


	private: System::Windows::Forms::Button^  N_Five;

	private: System::Windows::Forms::Button^  N_Four;
	private: System::Windows::Forms::Button^  N_Nine;


	private: System::Windows::Forms::Button^  N_Eight;

	private: System::Windows::Forms::Button^  N_Seven;
	private: System::Windows::Forms::Button^  N_Com;



	private: System::Windows::Forms::Button^  mul;

	private: System::Windows::Forms::Button^  minus;
	private: System::Windows::Forms::Button^  N_plus;


	private: System::Windows::Forms::Button^  N_Mod;


	private: System::Windows::Forms::Button^  N_Div;
	private: System::Windows::Forms::Button^  N_LCM;


	private: System::Windows::Forms::Button^  N_GCF;
	private: System::Windows::Forms::Button^  clr_N;

	private: System::Windows::Forms::Button^  equal;
	private: System::Windows::Forms::Button^  N_Comma;
	private: System::Windows::Forms::Button^  DeleteLast;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  Z_EQUAL;
	private: System::Windows::Forms::Button^  Z_DLTLAST;
	private: System::Windows::Forms::Button^  Z_CLR;
	private: System::Windows::Forms::Button^  Z_MINUSONE;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  Z_COMMA;

	private: System::Windows::Forms::Button^  Z_ADDONE;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::Button^  button11;
	private: System::Windows::Forms::Button^  button12;
	private: System::Windows::Forms::Button^  button13;
	private: System::Windows::Forms::Button^  button14;
	private: System::Windows::Forms::Button^  button15;
	private: System::Windows::Forms::Button^  Z_NOK;
	private: System::Windows::Forms::Button^  Z_COM;
	private: System::Windows::Forms::Button^  Z_NOD;
	private: System::Windows::Forms::Button^  Z_MUL;
	private: System::Windows::Forms::Button^  Z_MOD;
	private: System::Windows::Forms::Button^  Z_MINUS;
	private: System::Windows::Forms::Button^  Z_DIV;
	private: System::Windows::Forms::Button^  Z_PLUS;
	private: System::Windows::Forms::Button^  button24;
private: System::Windows::Forms::Button^  Z_CHNG;
private: System::Windows::Forms::TextBox^  Z_output;
private: System::Windows::Forms::TabPage^  tabPage3;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::Button^  Q_EQUAL;

private: System::Windows::Forms::Button^  Q_DLTLST;
private: System::Windows::Forms::Button^  Q_CLR;


private: System::Windows::Forms::Button^  Q_zero;
private: System::Windows::Forms::Button^  Q_CHNG;
private: System::Windows::Forms::Button^  Q_COMMA;




private: System::Windows::Forms::Button^  Q_three;


private: System::Windows::Forms::Button^  Q_two;

private: System::Windows::Forms::Button^  Q_one;
private: System::Windows::Forms::Button^  Q_six;


private: System::Windows::Forms::Button^  Q_five;

private: System::Windows::Forms::Button^  Q_four;
private: System::Windows::Forms::Button^  Q_nine;


private: System::Windows::Forms::Button^  Q_eight;
private: System::Windows::Forms::Button^  Q_COM;
private: System::Windows::Forms::Button^  Q_MUL;





private: System::Windows::Forms::Button^  Q_MINUS;


private: System::Windows::Forms::Button^  Q_PLUS;


private: System::Windows::Forms::Button^  Q_seven;

private: System::Windows::Forms::TextBox^  Q_output;
private: System::Windows::Forms::Button^  Q_DENOM;
private: System::Windows::Forms::TabPage^  tabPage4;
private: System::Windows::Forms::TextBox^  P_input;
private: System::Windows::Forms::RichTextBox^  P_output;
private: System::Windows::Forms::Button^  P_input_button;
private: System::Windows::Forms::Button^  P_clr;

















	protected:























	protected:

	private:
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Main::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->справкаToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->основныеВозможнстиToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->форматВводаToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->оПрограммеToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->NN_output = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->equal = (gcnew System::Windows::Forms::Button());
			this->DeleteLast = (gcnew System::Windows::Forms::Button());
			this->clr_N = (gcnew System::Windows::Forms::Button());
			this->N_SubOne = (gcnew System::Windows::Forms::Button());
			this->N_Zero = (gcnew System::Windows::Forms::Button());
			this->N_Comma = (gcnew System::Windows::Forms::Button());
			this->N_AddOne = (gcnew System::Windows::Forms::Button());
			this->N_Three = (gcnew System::Windows::Forms::Button());
			this->N_Two = (gcnew System::Windows::Forms::Button());
			this->N_One = (gcnew System::Windows::Forms::Button());
			this->N_Six = (gcnew System::Windows::Forms::Button());
			this->N_Five = (gcnew System::Windows::Forms::Button());
			this->N_Four = (gcnew System::Windows::Forms::Button());
			this->N_Nine = (gcnew System::Windows::Forms::Button());
			this->N_Eight = (gcnew System::Windows::Forms::Button());
			this->N_LCM = (gcnew System::Windows::Forms::Button());
			this->N_Com = (gcnew System::Windows::Forms::Button());
			this->N_GCF = (gcnew System::Windows::Forms::Button());
			this->mul = (gcnew System::Windows::Forms::Button());
			this->N_Mod = (gcnew System::Windows::Forms::Button());
			this->minus = (gcnew System::Windows::Forms::Button());
			this->N_Div = (gcnew System::Windows::Forms::Button());
			this->N_plus = (gcnew System::Windows::Forms::Button());
			this->N_Seven = (gcnew System::Windows::Forms::Button());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->Z_EQUAL = (gcnew System::Windows::Forms::Button());
			this->Z_DLTLAST = (gcnew System::Windows::Forms::Button());
			this->Z_CLR = (gcnew System::Windows::Forms::Button());
			this->Z_MINUSONE = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->Z_CHNG = (gcnew System::Windows::Forms::Button());
			this->Z_COMMA = (gcnew System::Windows::Forms::Button());
			this->Z_ADDONE = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->Z_NOK = (gcnew System::Windows::Forms::Button());
			this->Z_COM = (gcnew System::Windows::Forms::Button());
			this->Z_NOD = (gcnew System::Windows::Forms::Button());
			this->Z_MUL = (gcnew System::Windows::Forms::Button());
			this->Z_MOD = (gcnew System::Windows::Forms::Button());
			this->Z_MINUS = (gcnew System::Windows::Forms::Button());
			this->Z_DIV = (gcnew System::Windows::Forms::Button());
			this->Z_PLUS = (gcnew System::Windows::Forms::Button());
			this->button24 = (gcnew System::Windows::Forms::Button());
			this->Z_output = (gcnew System::Windows::Forms::TextBox());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->Q_EQUAL = (gcnew System::Windows::Forms::Button());
			this->Q_DLTLST = (gcnew System::Windows::Forms::Button());
			this->Q_CLR = (gcnew System::Windows::Forms::Button());
			this->Q_zero = (gcnew System::Windows::Forms::Button());
			this->Q_CHNG = (gcnew System::Windows::Forms::Button());
			this->Q_COMMA = (gcnew System::Windows::Forms::Button());
			this->Q_three = (gcnew System::Windows::Forms::Button());
			this->Q_two = (gcnew System::Windows::Forms::Button());
			this->Q_one = (gcnew System::Windows::Forms::Button());
			this->Q_six = (gcnew System::Windows::Forms::Button());
			this->Q_five = (gcnew System::Windows::Forms::Button());
			this->Q_four = (gcnew System::Windows::Forms::Button());
			this->Q_nine = (gcnew System::Windows::Forms::Button());
			this->Q_eight = (gcnew System::Windows::Forms::Button());
			this->Q_COM = (gcnew System::Windows::Forms::Button());
			this->Q_DENOM = (gcnew System::Windows::Forms::Button());
			this->Q_MUL = (gcnew System::Windows::Forms::Button());
			this->Q_MINUS = (gcnew System::Windows::Forms::Button());
			this->Q_PLUS = (gcnew System::Windows::Forms::Button());
			this->Q_seven = (gcnew System::Windows::Forms::Button());
			this->Q_output = (gcnew System::Windows::Forms::TextBox());
			this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
			this->P_input_button = (gcnew System::Windows::Forms::Button());
			this->P_output = (gcnew System::Windows::Forms::RichTextBox());
			this->P_input = (gcnew System::Windows::Forms::TextBox());
			this->P_clr = (gcnew System::Windows::Forms::Button());
			this->menuStrip1->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->tabPage3->SuspendLayout();
			this->tabPage4->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->справкаToolStripMenuItem,
					this->оПрограммеToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(690, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// справкаToolStripMenuItem
			// 
			this->справкаToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->основныеВозможнстиToolStripMenuItem,
					this->форматВводаToolStripMenuItem
			});
			this->справкаToolStripMenuItem->Name = L"справкаToolStripMenuItem";
			this->справкаToolStripMenuItem->Size = System::Drawing::Size(65, 20);
			this->справкаToolStripMenuItem->Text = L"Справка";
			// 
			// основныеВозможнстиToolStripMenuItem
			// 
			this->основныеВозможнстиToolStripMenuItem->Name = L"основныеВозможнстиToolStripMenuItem";
			this->основныеВозможнстиToolStripMenuItem->Size = System::Drawing::Size(202, 22);
			this->основныеВозможнстиToolStripMenuItem->Text = L"Основные возможнсти";
			// 
			// форматВводаToolStripMenuItem
			// 
			this->форматВводаToolStripMenuItem->Name = L"форматВводаToolStripMenuItem";
			this->форматВводаToolStripMenuItem->Size = System::Drawing::Size(202, 22);
			this->форматВводаToolStripMenuItem->Text = L"Формат ввода";
			// 
			// оПрограммеToolStripMenuItem
			// 
			this->оПрограммеToolStripMenuItem->Name = L"оПрограммеToolStripMenuItem";
			this->оПрограммеToolStripMenuItem->Size = System::Drawing::Size(94, 20);
			this->оПрограммеToolStripMenuItem->Text = L"О программе";
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Controls->Add(this->tabPage4);
			this->tabControl1->Location = System::Drawing::Point(0, 27);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(699, 442);
			this->tabControl1->TabIndex = 1;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->NN_output);
			this->tabPage1->Controls->Add(this->label1);
			this->tabPage1->Controls->Add(this->equal);
			this->tabPage1->Controls->Add(this->DeleteLast);
			this->tabPage1->Controls->Add(this->clr_N);
			this->tabPage1->Controls->Add(this->N_SubOne);
			this->tabPage1->Controls->Add(this->N_Zero);
			this->tabPage1->Controls->Add(this->N_Comma);
			this->tabPage1->Controls->Add(this->N_AddOne);
			this->tabPage1->Controls->Add(this->N_Three);
			this->tabPage1->Controls->Add(this->N_Two);
			this->tabPage1->Controls->Add(this->N_One);
			this->tabPage1->Controls->Add(this->N_Six);
			this->tabPage1->Controls->Add(this->N_Five);
			this->tabPage1->Controls->Add(this->N_Four);
			this->tabPage1->Controls->Add(this->N_Nine);
			this->tabPage1->Controls->Add(this->N_Eight);
			this->tabPage1->Controls->Add(this->N_LCM);
			this->tabPage1->Controls->Add(this->N_Com);
			this->tabPage1->Controls->Add(this->N_GCF);
			this->tabPage1->Controls->Add(this->mul);
			this->tabPage1->Controls->Add(this->N_Mod);
			this->tabPage1->Controls->Add(this->minus);
			this->tabPage1->Controls->Add(this->N_Div);
			this->tabPage1->Controls->Add(this->N_plus);
			this->tabPage1->Controls->Add(this->N_Seven);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(691, 416);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"Натуральные числа";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// NN_output
			// 
			this->NN_output->BackColor = System::Drawing::SystemColors::Window;
			this->NN_output->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->NN_output->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NN_output->Location = System::Drawing::Point(6, 6);
			this->NN_output->Name = L"NN_output";
			this->NN_output->ReadOnly = true;
			this->NN_output->Size = System::Drawing::Size(668, 44);
			this->NN_output->TabIndex = 0;
			this->NN_output->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label1->Location = System::Drawing::Point(6, 69);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(348, 306);
			this->label1->TabIndex = 4;
			this->label1->Text = resources->GetString(L"label1.Text");
			// 
			// equal
			// 
			this->equal->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->equal->Location = System::Drawing::Point(564, 301);
			this->equal->Name = L"equal";
			this->equal->Size = System::Drawing::Size(110, 52);
			this->equal->TabIndex = 3;
			this->equal->Text = L"=";
			this->equal->UseVisualStyleBackColor = true;
			this->equal->Click += gcnew System::EventHandler(this, &Main::equal_Click);
			// 
			// DeleteLast
			// 
			this->DeleteLast->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->DeleteLast->Location = System::Drawing::Point(448, 301);
			this->DeleteLast->Name = L"DeleteLast";
			this->DeleteLast->Size = System::Drawing::Size(52, 52);
			this->DeleteLast->TabIndex = 3;
			this->DeleteLast->Text = L"<";
			this->DeleteLast->UseVisualStyleBackColor = true;
			this->DeleteLast->Click += gcnew System::EventHandler(this, &Main::DeleteLast_Click);
			// 
			// clr_N
			// 
			this->clr_N->Location = System::Drawing::Point(390, 301);
			this->clr_N->Name = L"clr_N";
			this->clr_N->Size = System::Drawing::Size(52, 52);
			this->clr_N->TabIndex = 3;
			this->clr_N->Text = L"Clear";
			this->clr_N->UseVisualStyleBackColor = true;
			this->clr_N->Click += gcnew System::EventHandler(this, &Main::clr_N_Click);
			// 
			// N_SubOne
			// 
			this->N_SubOne->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_SubOne->Location = System::Drawing::Point(622, 243);
			this->N_SubOne->Name = L"N_SubOne";
			this->N_SubOne->Size = System::Drawing::Size(52, 52);
			this->N_SubOne->TabIndex = 1;
			this->N_SubOne->Text = L"--";
			this->N_SubOne->UseVisualStyleBackColor = true;
			this->N_SubOne->Click += gcnew System::EventHandler(this, &Main::N_SubOne_Click);
			// 
			// N_Zero
			// 
			this->N_Zero->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Zero->Location = System::Drawing::Point(564, 243);
			this->N_Zero->Name = L"N_Zero";
			this->N_Zero->Size = System::Drawing::Size(52, 52);
			this->N_Zero->TabIndex = 1;
			this->N_Zero->Text = L"0";
			this->N_Zero->UseVisualStyleBackColor = true;
			this->N_Zero->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Comma
			// 
			this->N_Comma->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Comma->Location = System::Drawing::Point(506, 301);
			this->N_Comma->Name = L"N_Comma";
			this->N_Comma->Size = System::Drawing::Size(52, 52);
			this->N_Comma->TabIndex = 1;
			this->N_Comma->Text = L",";
			this->N_Comma->UseVisualStyleBackColor = true;
			this->N_Comma->Click += gcnew System::EventHandler(this, &Main::N_Comma_Click);
			// 
			// N_AddOne
			// 
			this->N_AddOne->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_AddOne->Location = System::Drawing::Point(506, 243);
			this->N_AddOne->Name = L"N_AddOne";
			this->N_AddOne->Size = System::Drawing::Size(52, 52);
			this->N_AddOne->TabIndex = 1;
			this->N_AddOne->Text = L"++";
			this->N_AddOne->UseVisualStyleBackColor = true;
			this->N_AddOne->Click += gcnew System::EventHandler(this, &Main::N_AddOne_Click);
			// 
			// N_Three
			// 
			this->N_Three->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Three->Location = System::Drawing::Point(622, 185);
			this->N_Three->Name = L"N_Three";
			this->N_Three->Size = System::Drawing::Size(52, 52);
			this->N_Three->TabIndex = 1;
			this->N_Three->Text = L"3";
			this->N_Three->UseVisualStyleBackColor = true;
			this->N_Three->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Two
			// 
			this->N_Two->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Two->Location = System::Drawing::Point(564, 185);
			this->N_Two->Name = L"N_Two";
			this->N_Two->Size = System::Drawing::Size(52, 52);
			this->N_Two->TabIndex = 1;
			this->N_Two->Text = L"2";
			this->N_Two->UseVisualStyleBackColor = true;
			this->N_Two->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_One
			// 
			this->N_One->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_One->Location = System::Drawing::Point(506, 185);
			this->N_One->Name = L"N_One";
			this->N_One->Size = System::Drawing::Size(52, 52);
			this->N_One->TabIndex = 1;
			this->N_One->Text = L"1";
			this->N_One->UseVisualStyleBackColor = true;
			this->N_One->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Six
			// 
			this->N_Six->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Six->Location = System::Drawing::Point(622, 127);
			this->N_Six->Name = L"N_Six";
			this->N_Six->Size = System::Drawing::Size(52, 52);
			this->N_Six->TabIndex = 1;
			this->N_Six->Text = L"6";
			this->N_Six->UseVisualStyleBackColor = true;
			this->N_Six->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Five
			// 
			this->N_Five->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Five->Location = System::Drawing::Point(564, 127);
			this->N_Five->Name = L"N_Five";
			this->N_Five->Size = System::Drawing::Size(52, 52);
			this->N_Five->TabIndex = 1;
			this->N_Five->Text = L"5";
			this->N_Five->UseVisualStyleBackColor = true;
			this->N_Five->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Four
			// 
			this->N_Four->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Four->Location = System::Drawing::Point(506, 127);
			this->N_Four->Name = L"N_Four";
			this->N_Four->Size = System::Drawing::Size(52, 52);
			this->N_Four->TabIndex = 1;
			this->N_Four->Text = L"4";
			this->N_Four->UseVisualStyleBackColor = true;
			this->N_Four->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Nine
			// 
			this->N_Nine->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Nine->Location = System::Drawing::Point(622, 69);
			this->N_Nine->Name = L"N_Nine";
			this->N_Nine->Size = System::Drawing::Size(52, 52);
			this->N_Nine->TabIndex = 1;
			this->N_Nine->Text = L"9";
			this->N_Nine->UseVisualStyleBackColor = true;
			this->N_Nine->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_Eight
			// 
			this->N_Eight->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Eight->Location = System::Drawing::Point(564, 69);
			this->N_Eight->Name = L"N_Eight";
			this->N_Eight->Size = System::Drawing::Size(52, 52);
			this->N_Eight->TabIndex = 1;
			this->N_Eight->Text = L"8";
			this->N_Eight->UseVisualStyleBackColor = true;
			this->N_Eight->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// N_LCM
			// 
			this->N_LCM->Location = System::Drawing::Point(390, 243);
			this->N_LCM->Name = L"N_LCM";
			this->N_LCM->Size = System::Drawing::Size(52, 52);
			this->N_LCM->TabIndex = 1;
			this->N_LCM->Text = L"НОК";
			this->N_LCM->UseVisualStyleBackColor = true;
			this->N_LCM->Click += gcnew System::EventHandler(this, &Main::N_LCM_Click);
			// 
			// N_Com
			// 
			this->N_Com->Location = System::Drawing::Point(448, 243);
			this->N_Com->Name = L"N_Com";
			this->N_Com->Size = System::Drawing::Size(52, 52);
			this->N_Com->TabIndex = 1;
			this->N_Com->Text = L"COM";
			this->N_Com->UseVisualStyleBackColor = true;
			this->N_Com->Click += gcnew System::EventHandler(this, &Main::N_Com_Click);
			// 
			// N_GCF
			// 
			this->N_GCF->Location = System::Drawing::Point(390, 185);
			this->N_GCF->Name = L"N_GCF";
			this->N_GCF->Size = System::Drawing::Size(52, 52);
			this->N_GCF->TabIndex = 1;
			this->N_GCF->Text = L"НОД";
			this->N_GCF->UseVisualStyleBackColor = true;
			this->N_GCF->Click += gcnew System::EventHandler(this, &Main::N_GCF_Click);
			// 
			// mul
			// 
			this->mul->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->mul->Location = System::Drawing::Point(448, 185);
			this->mul->Name = L"mul";
			this->mul->Size = System::Drawing::Size(52, 52);
			this->mul->TabIndex = 2;
			this->mul->Text = L"*";
			this->mul->Click += gcnew System::EventHandler(this, &Main::WriteOpertor_N);
			// 
			// N_Mod
			// 
			this->N_Mod->Location = System::Drawing::Point(390, 127);
			this->N_Mod->Name = L"N_Mod";
			this->N_Mod->Size = System::Drawing::Size(52, 52);
			this->N_Mod->TabIndex = 1;
			this->N_Mod->Text = L"MOD";
			this->N_Mod->UseVisualStyleBackColor = true;
			this->N_Mod->Click += gcnew System::EventHandler(this, &Main::WriteOpertor_N);
			// 
			// minus
			// 
			this->minus->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->minus->Location = System::Drawing::Point(448, 127);
			this->minus->Name = L"minus";
			this->minus->Size = System::Drawing::Size(52, 52);
			this->minus->TabIndex = 1;
			this->minus->Text = L"-";
			this->minus->UseVisualStyleBackColor = true;
			this->minus->Click += gcnew System::EventHandler(this, &Main::WriteOpertor_N);
			// 
			// N_Div
			// 
			this->N_Div->Location = System::Drawing::Point(390, 69);
			this->N_Div->Name = L"N_Div";
			this->N_Div->Size = System::Drawing::Size(52, 52);
			this->N_Div->TabIndex = 1;
			this->N_Div->Text = L"DIV";
			this->N_Div->UseVisualStyleBackColor = true;
			this->N_Div->Click += gcnew System::EventHandler(this, &Main::WriteOpertor_N);
			// 
			// N_plus
			// 
			this->N_plus->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_plus->Location = System::Drawing::Point(448, 69);
			this->N_plus->Name = L"N_plus";
			this->N_plus->Size = System::Drawing::Size(52, 52);
			this->N_plus->TabIndex = 1;
			this->N_plus->Text = L"+";
			this->N_plus->UseVisualStyleBackColor = true;
			this->N_plus->Click += gcnew System::EventHandler(this, &Main::WriteOpertor_N);
			// 
			// N_Seven
			// 
			this->N_Seven->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->N_Seven->Location = System::Drawing::Point(506, 69);
			this->N_Seven->Name = L"N_Seven";
			this->N_Seven->Size = System::Drawing::Size(52, 52);
			this->N_Seven->TabIndex = 1;
			this->N_Seven->Text = L"7";
			this->N_Seven->UseVisualStyleBackColor = true;
			this->N_Seven->Click += gcnew System::EventHandler(this, &Main::Write_Number_N);
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->label2);
			this->tabPage2->Controls->Add(this->Z_EQUAL);
			this->tabPage2->Controls->Add(this->Z_DLTLAST);
			this->tabPage2->Controls->Add(this->Z_CLR);
			this->tabPage2->Controls->Add(this->Z_MINUSONE);
			this->tabPage2->Controls->Add(this->button5);
			this->tabPage2->Controls->Add(this->Z_CHNG);
			this->tabPage2->Controls->Add(this->Z_COMMA);
			this->tabPage2->Controls->Add(this->Z_ADDONE);
			this->tabPage2->Controls->Add(this->button8);
			this->tabPage2->Controls->Add(this->button9);
			this->tabPage2->Controls->Add(this->button10);
			this->tabPage2->Controls->Add(this->button11);
			this->tabPage2->Controls->Add(this->button12);
			this->tabPage2->Controls->Add(this->button13);
			this->tabPage2->Controls->Add(this->button14);
			this->tabPage2->Controls->Add(this->button15);
			this->tabPage2->Controls->Add(this->Z_NOK);
			this->tabPage2->Controls->Add(this->Z_COM);
			this->tabPage2->Controls->Add(this->Z_NOD);
			this->tabPage2->Controls->Add(this->Z_MUL);
			this->tabPage2->Controls->Add(this->Z_MOD);
			this->tabPage2->Controls->Add(this->Z_MINUS);
			this->tabPage2->Controls->Add(this->Z_DIV);
			this->tabPage2->Controls->Add(this->Z_PLUS);
			this->tabPage2->Controls->Add(this->button24);
			this->tabPage2->Controls->Add(this->Z_output);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(691, 416);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Целые числа";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label2->Location = System::Drawing::Point(6, 69);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(348, 289);
			this->label2->TabIndex = 30;
			this->label2->Text = resources->GetString(L"label2.Text");
			// 
			// Z_EQUAL
			// 
			this->Z_EQUAL->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_EQUAL->Location = System::Drawing::Point(622, 301);
			this->Z_EQUAL->Name = L"Z_EQUAL";
			this->Z_EQUAL->Size = System::Drawing::Size(52, 52);
			this->Z_EQUAL->TabIndex = 29;
			this->Z_EQUAL->Text = L"=";
			this->Z_EQUAL->UseVisualStyleBackColor = true;
			this->Z_EQUAL->Click += gcnew System::EventHandler(this, &Main::Z_EQUAL_Click);
			// 
			// Z_DLTLAST
			// 
			this->Z_DLTLAST->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_DLTLAST->Location = System::Drawing::Point(448, 301);
			this->Z_DLTLAST->Name = L"Z_DLTLAST";
			this->Z_DLTLAST->Size = System::Drawing::Size(52, 52);
			this->Z_DLTLAST->TabIndex = 28;
			this->Z_DLTLAST->Text = L"<";
			this->Z_DLTLAST->UseVisualStyleBackColor = true;
			this->Z_DLTLAST->Click += gcnew System::EventHandler(this, &Main::Z_DLTLAST_Click);
			// 
			// Z_CLR
			// 
			this->Z_CLR->Location = System::Drawing::Point(390, 301);
			this->Z_CLR->Name = L"Z_CLR";
			this->Z_CLR->Size = System::Drawing::Size(52, 52);
			this->Z_CLR->TabIndex = 27;
			this->Z_CLR->Text = L"Clear";
			this->Z_CLR->UseVisualStyleBackColor = true;
			this->Z_CLR->Click += gcnew System::EventHandler(this, &Main::Z_CLR_Click);
			// 
			// Z_MINUSONE
			// 
			this->Z_MINUSONE->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_MINUSONE->Location = System::Drawing::Point(622, 243);
			this->Z_MINUSONE->Name = L"Z_MINUSONE";
			this->Z_MINUSONE->Size = System::Drawing::Size(52, 52);
			this->Z_MINUSONE->TabIndex = 24;
			this->Z_MINUSONE->Text = L"--";
			this->Z_MINUSONE->UseVisualStyleBackColor = true;
			this->Z_MINUSONE->Click += gcnew System::EventHandler(this, &Main::Z_MINUSONE_Click);
			// 
			// button5
			// 
			this->button5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button5->Location = System::Drawing::Point(564, 243);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(52, 52);
			this->button5->TabIndex = 23;
			this->button5->Text = L"0";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Main::button5_Click);
			// 
			// Z_CHNG
			// 
			this->Z_CHNG->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_CHNG->Location = System::Drawing::Point(564, 301);
			this->Z_CHNG->Name = L"Z_CHNG";
			this->Z_CHNG->Size = System::Drawing::Size(52, 52);
			this->Z_CHNG->TabIndex = 22;
			this->Z_CHNG->Text = L"±";
			this->Z_CHNG->UseVisualStyleBackColor = true;
			this->Z_CHNG->Click += gcnew System::EventHandler(this, &Main::Z_CHNG_Click);
			// 
			// Z_COMMA
			// 
			this->Z_COMMA->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_COMMA->Location = System::Drawing::Point(506, 301);
			this->Z_COMMA->Name = L"Z_COMMA";
			this->Z_COMMA->Size = System::Drawing::Size(52, 52);
			this->Z_COMMA->TabIndex = 22;
			this->Z_COMMA->Text = L",";
			this->Z_COMMA->UseVisualStyleBackColor = true;
			this->Z_COMMA->Click += gcnew System::EventHandler(this, &Main::Z_COMMA_Click);
			// 
			// Z_ADDONE
			// 
			this->Z_ADDONE->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_ADDONE->Location = System::Drawing::Point(506, 243);
			this->Z_ADDONE->Name = L"Z_ADDONE";
			this->Z_ADDONE->Size = System::Drawing::Size(52, 52);
			this->Z_ADDONE->TabIndex = 21;
			this->Z_ADDONE->Text = L"++";
			this->Z_ADDONE->UseVisualStyleBackColor = true;
			this->Z_ADDONE->Click += gcnew System::EventHandler(this, &Main::Z_ADDONE_Click);
			// 
			// button8
			// 
			this->button8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button8->Location = System::Drawing::Point(622, 185);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(52, 52);
			this->button8->TabIndex = 20;
			this->button8->Text = L"3";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &Main::button8_Click);
			// 
			// button9
			// 
			this->button9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button9->Location = System::Drawing::Point(564, 185);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(52, 52);
			this->button9->TabIndex = 19;
			this->button9->Text = L"2";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &Main::button9_Click);
			// 
			// button10
			// 
			this->button10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button10->Location = System::Drawing::Point(506, 185);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(52, 52);
			this->button10->TabIndex = 18;
			this->button10->Text = L"1";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &Main::button10_Click);
			// 
			// button11
			// 
			this->button11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button11->Location = System::Drawing::Point(622, 127);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(52, 52);
			this->button11->TabIndex = 25;
			this->button11->Text = L"6";
			this->button11->UseVisualStyleBackColor = true;
			this->button11->Click += gcnew System::EventHandler(this, &Main::button11_Click);
			// 
			// button12
			// 
			this->button12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button12->Location = System::Drawing::Point(564, 127);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(52, 52);
			this->button12->TabIndex = 17;
			this->button12->Text = L"5";
			this->button12->UseVisualStyleBackColor = true;
			this->button12->Click += gcnew System::EventHandler(this, &Main::button12_Click);
			// 
			// button13
			// 
			this->button13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button13->Location = System::Drawing::Point(506, 127);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(52, 52);
			this->button13->TabIndex = 15;
			this->button13->Text = L"4";
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &Main::button13_Click);
			// 
			// button14
			// 
			this->button14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button14->Location = System::Drawing::Point(622, 69);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(52, 52);
			this->button14->TabIndex = 14;
			this->button14->Text = L"9";
			this->button14->UseVisualStyleBackColor = true;
			this->button14->Click += gcnew System::EventHandler(this, &Main::button14_Click_1);
			// 
			// button15
			// 
			this->button15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button15->Location = System::Drawing::Point(564, 69);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(52, 52);
			this->button15->TabIndex = 13;
			this->button15->Text = L"8";
			this->button15->UseVisualStyleBackColor = true;
			this->button15->Click += gcnew System::EventHandler(this, &Main::button15_Click);
			// 
			// Z_NOK
			// 
			this->Z_NOK->Location = System::Drawing::Point(390, 243);
			this->Z_NOK->Name = L"Z_NOK";
			this->Z_NOK->Size = System::Drawing::Size(52, 52);
			this->Z_NOK->TabIndex = 12;
			this->Z_NOK->Text = L"НОК";
			this->Z_NOK->UseVisualStyleBackColor = true;
			this->Z_NOK->Click += gcnew System::EventHandler(this, &Main::Z_NOK_Click);
			// 
			// Z_COM
			// 
			this->Z_COM->Location = System::Drawing::Point(448, 243);
			this->Z_COM->Name = L"Z_COM";
			this->Z_COM->Size = System::Drawing::Size(52, 52);
			this->Z_COM->TabIndex = 11;
			this->Z_COM->Text = L"COM";
			this->Z_COM->UseVisualStyleBackColor = true;
			this->Z_COM->Click += gcnew System::EventHandler(this, &Main::Z_COM_Click);
			// 
			// Z_NOD
			// 
			this->Z_NOD->Location = System::Drawing::Point(390, 185);
			this->Z_NOD->Name = L"Z_NOD";
			this->Z_NOD->Size = System::Drawing::Size(52, 52);
			this->Z_NOD->TabIndex = 10;
			this->Z_NOD->Text = L"НОД";
			this->Z_NOD->UseVisualStyleBackColor = true;
			this->Z_NOD->Click += gcnew System::EventHandler(this, &Main::Z_NOD_Click);
			// 
			// Z_MUL
			// 
			this->Z_MUL->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_MUL->Location = System::Drawing::Point(448, 185);
			this->Z_MUL->Name = L"Z_MUL";
			this->Z_MUL->Size = System::Drawing::Size(52, 52);
			this->Z_MUL->TabIndex = 26;
			this->Z_MUL->Text = L"*";
			this->Z_MUL->Click += gcnew System::EventHandler(this, &Main::Z_MUL_Click);
			// 
			// Z_MOD
			// 
			this->Z_MOD->Location = System::Drawing::Point(390, 127);
			this->Z_MOD->Name = L"Z_MOD";
			this->Z_MOD->Size = System::Drawing::Size(52, 52);
			this->Z_MOD->TabIndex = 9;
			this->Z_MOD->Text = L"MOD";
			this->Z_MOD->UseVisualStyleBackColor = true;
			this->Z_MOD->Click += gcnew System::EventHandler(this, &Main::Z_MOD_Click);
			// 
			// Z_MINUS
			// 
			this->Z_MINUS->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_MINUS->Location = System::Drawing::Point(448, 127);
			this->Z_MINUS->Name = L"Z_MINUS";
			this->Z_MINUS->Size = System::Drawing::Size(52, 52);
			this->Z_MINUS->TabIndex = 8;
			this->Z_MINUS->Text = L"-";
			this->Z_MINUS->UseVisualStyleBackColor = true;
			this->Z_MINUS->Click += gcnew System::EventHandler(this, &Main::Z_MINUS_Click);
			// 
			// Z_DIV
			// 
			this->Z_DIV->Location = System::Drawing::Point(390, 69);
			this->Z_DIV->Name = L"Z_DIV";
			this->Z_DIV->Size = System::Drawing::Size(52, 52);
			this->Z_DIV->TabIndex = 7;
			this->Z_DIV->Text = L"DIV";
			this->Z_DIV->UseVisualStyleBackColor = true;
			this->Z_DIV->Click += gcnew System::EventHandler(this, &Main::Z_DIV_Click);
			// 
			// Z_PLUS
			// 
			this->Z_PLUS->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Z_PLUS->Location = System::Drawing::Point(448, 69);
			this->Z_PLUS->Name = L"Z_PLUS";
			this->Z_PLUS->Size = System::Drawing::Size(52, 52);
			this->Z_PLUS->TabIndex = 6;
			this->Z_PLUS->Text = L"+";
			this->Z_PLUS->UseVisualStyleBackColor = true;
			this->Z_PLUS->Click += gcnew System::EventHandler(this, &Main::Z_PLUS_Click);
			// 
			// button24
			// 
			this->button24->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->button24->Location = System::Drawing::Point(506, 69);
			this->button24->Name = L"button24";
			this->button24->Size = System::Drawing::Size(52, 52);
			this->button24->TabIndex = 16;
			this->button24->Text = L"7";
			this->button24->UseVisualStyleBackColor = true;
			this->button24->Click += gcnew System::EventHandler(this, &Main::button24_Click);
			// 
			// Z_output
			// 
			this->Z_output->BackColor = System::Drawing::SystemColors::Window;
			this->Z_output->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->Z_output->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Z_output->Location = System::Drawing::Point(6, 6);
			this->Z_output->Name = L"Z_output";
			this->Z_output->ReadOnly = true;
			this->Z_output->Size = System::Drawing::Size(668, 44);
			this->Z_output->TabIndex = 5;
			this->Z_output->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// tabPage3
			// 
			this->tabPage3->Controls->Add(this->label3);
			this->tabPage3->Controls->Add(this->Q_EQUAL);
			this->tabPage3->Controls->Add(this->Q_DLTLST);
			this->tabPage3->Controls->Add(this->Q_CLR);
			this->tabPage3->Controls->Add(this->Q_zero);
			this->tabPage3->Controls->Add(this->Q_CHNG);
			this->tabPage3->Controls->Add(this->Q_COMMA);
			this->tabPage3->Controls->Add(this->Q_three);
			this->tabPage3->Controls->Add(this->Q_two);
			this->tabPage3->Controls->Add(this->Q_one);
			this->tabPage3->Controls->Add(this->Q_six);
			this->tabPage3->Controls->Add(this->Q_five);
			this->tabPage3->Controls->Add(this->Q_four);
			this->tabPage3->Controls->Add(this->Q_nine);
			this->tabPage3->Controls->Add(this->Q_eight);
			this->tabPage3->Controls->Add(this->Q_COM);
			this->tabPage3->Controls->Add(this->Q_DENOM);
			this->tabPage3->Controls->Add(this->Q_MUL);
			this->tabPage3->Controls->Add(this->Q_MINUS);
			this->tabPage3->Controls->Add(this->Q_PLUS);
			this->tabPage3->Controls->Add(this->Q_seven);
			this->tabPage3->Controls->Add(this->Q_output);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Padding = System::Windows::Forms::Padding(3);
			this->tabPage3->Size = System::Drawing::Size(691, 416);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Рациональные числа";
			this->tabPage3->UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->label3->Location = System::Drawing::Point(6, 69);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(369, 204);
			this->label3->TabIndex = 57;
			this->label3->Text = resources->GetString(L"label3.Text");
			// 
			// Q_EQUAL
			// 
			this->Q_EQUAL->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_EQUAL->Location = System::Drawing::Point(622, 301);
			this->Q_EQUAL->Name = L"Q_EQUAL";
			this->Q_EQUAL->Size = System::Drawing::Size(52, 52);
			this->Q_EQUAL->TabIndex = 56;
			this->Q_EQUAL->Text = L"=";
			this->Q_EQUAL->UseVisualStyleBackColor = true;
			this->Q_EQUAL->Click += gcnew System::EventHandler(this, &Main::Q_EQUAL_Click);
			// 
			// Q_DLTLST
			// 
			this->Q_DLTLST->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_DLTLST->Location = System::Drawing::Point(448, 301);
			this->Q_DLTLST->Name = L"Q_DLTLST";
			this->Q_DLTLST->Size = System::Drawing::Size(52, 52);
			this->Q_DLTLST->TabIndex = 55;
			this->Q_DLTLST->Text = L"<";
			this->Q_DLTLST->UseVisualStyleBackColor = true;
			this->Q_DLTLST->Click += gcnew System::EventHandler(this, &Main::Q_DLTLST_Click);
			// 
			// Q_CLR
			// 
			this->Q_CLR->Location = System::Drawing::Point(506, 301);
			this->Q_CLR->Name = L"Q_CLR";
			this->Q_CLR->Size = System::Drawing::Size(52, 52);
			this->Q_CLR->TabIndex = 54;
			this->Q_CLR->Text = L"Clear";
			this->Q_CLR->UseVisualStyleBackColor = true;
			this->Q_CLR->Click += gcnew System::EventHandler(this, &Main::Q_CLR_Click);
			// 
			// Q_zero
			// 
			this->Q_zero->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_zero->Location = System::Drawing::Point(564, 243);
			this->Q_zero->Name = L"Q_zero";
			this->Q_zero->Size = System::Drawing::Size(52, 52);
			this->Q_zero->TabIndex = 50;
			this->Q_zero->Text = L"0";
			this->Q_zero->UseVisualStyleBackColor = true;
			this->Q_zero->Click += gcnew System::EventHandler(this, &Main::Q_zero_Click);
			// 
			// Q_CHNG
			// 
			this->Q_CHNG->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_CHNG->Location = System::Drawing::Point(564, 301);
			this->Q_CHNG->Name = L"Q_CHNG";
			this->Q_CHNG->Size = System::Drawing::Size(52, 52);
			this->Q_CHNG->TabIndex = 49;
			this->Q_CHNG->Text = L"±";
			this->Q_CHNG->UseVisualStyleBackColor = true;
			this->Q_CHNG->Click += gcnew System::EventHandler(this, &Main::Q_CHNG_Click);
			// 
			// Q_COMMA
			// 
			this->Q_COMMA->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_COMMA->Location = System::Drawing::Point(620, 243);
			this->Q_COMMA->Name = L"Q_COMMA";
			this->Q_COMMA->Size = System::Drawing::Size(52, 52);
			this->Q_COMMA->TabIndex = 48;
			this->Q_COMMA->Text = L",";
			this->Q_COMMA->UseVisualStyleBackColor = true;
			this->Q_COMMA->Click += gcnew System::EventHandler(this, &Main::Q_COMMA_Click);
			// 
			// Q_three
			// 
			this->Q_three->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_three->Location = System::Drawing::Point(622, 185);
			this->Q_three->Name = L"Q_three";
			this->Q_three->Size = System::Drawing::Size(52, 52);
			this->Q_three->TabIndex = 46;
			this->Q_three->Text = L"3";
			this->Q_three->UseVisualStyleBackColor = true;
			this->Q_three->Click += gcnew System::EventHandler(this, &Main::Q_three_Click);
			// 
			// Q_two
			// 
			this->Q_two->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_two->Location = System::Drawing::Point(564, 185);
			this->Q_two->Name = L"Q_two";
			this->Q_two->Size = System::Drawing::Size(52, 52);
			this->Q_two->TabIndex = 45;
			this->Q_two->Text = L"2";
			this->Q_two->UseVisualStyleBackColor = true;
			this->Q_two->Click += gcnew System::EventHandler(this, &Main::Q_two_Click);
			// 
			// Q_one
			// 
			this->Q_one->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_one->Location = System::Drawing::Point(506, 185);
			this->Q_one->Name = L"Q_one";
			this->Q_one->Size = System::Drawing::Size(52, 52);
			this->Q_one->TabIndex = 44;
			this->Q_one->Text = L"1";
			this->Q_one->UseVisualStyleBackColor = true;
			this->Q_one->Click += gcnew System::EventHandler(this, &Main::Q_one_Click);
			// 
			// Q_six
			// 
			this->Q_six->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_six->Location = System::Drawing::Point(622, 127);
			this->Q_six->Name = L"Q_six";
			this->Q_six->Size = System::Drawing::Size(52, 52);
			this->Q_six->TabIndex = 52;
			this->Q_six->Text = L"6";
			this->Q_six->UseVisualStyleBackColor = true;
			this->Q_six->Click += gcnew System::EventHandler(this, &Main::Q_six_Click);
			// 
			// Q_five
			// 
			this->Q_five->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_five->Location = System::Drawing::Point(564, 127);
			this->Q_five->Name = L"Q_five";
			this->Q_five->Size = System::Drawing::Size(52, 52);
			this->Q_five->TabIndex = 43;
			this->Q_five->Text = L"5";
			this->Q_five->UseVisualStyleBackColor = true;
			this->Q_five->Click += gcnew System::EventHandler(this, &Main::Q_five_Click);
			// 
			// Q_four
			// 
			this->Q_four->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_four->Location = System::Drawing::Point(506, 127);
			this->Q_four->Name = L"Q_four";
			this->Q_four->Size = System::Drawing::Size(52, 52);
			this->Q_four->TabIndex = 41;
			this->Q_four->Text = L"4";
			this->Q_four->UseVisualStyleBackColor = true;
			this->Q_four->Click += gcnew System::EventHandler(this, &Main::Q_four_Click);
			// 
			// Q_nine
			// 
			this->Q_nine->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_nine->Location = System::Drawing::Point(622, 69);
			this->Q_nine->Name = L"Q_nine";
			this->Q_nine->Size = System::Drawing::Size(52, 52);
			this->Q_nine->TabIndex = 40;
			this->Q_nine->Text = L"9";
			this->Q_nine->UseVisualStyleBackColor = true;
			this->Q_nine->Click += gcnew System::EventHandler(this, &Main::Q_nine_Click);
			// 
			// Q_eight
			// 
			this->Q_eight->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_eight->Location = System::Drawing::Point(564, 69);
			this->Q_eight->Name = L"Q_eight";
			this->Q_eight->Size = System::Drawing::Size(52, 52);
			this->Q_eight->TabIndex = 39;
			this->Q_eight->Text = L"8";
			this->Q_eight->UseVisualStyleBackColor = true;
			this->Q_eight->Click += gcnew System::EventHandler(this, &Main::Q_eight_Click);
			// 
			// Q_COM
			// 
			this->Q_COM->Location = System::Drawing::Point(506, 243);
			this->Q_COM->Name = L"Q_COM";
			this->Q_COM->Size = System::Drawing::Size(52, 52);
			this->Q_COM->TabIndex = 37;
			this->Q_COM->Text = L"COM";
			this->Q_COM->UseVisualStyleBackColor = true;
			this->Q_COM->Click += gcnew System::EventHandler(this, &Main::Q_COM_Click);
			// 
			// Q_DENOM
			// 
			this->Q_DENOM->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_DENOM->Location = System::Drawing::Point(448, 243);
			this->Q_DENOM->Name = L"Q_DENOM";
			this->Q_DENOM->Size = System::Drawing::Size(52, 52);
			this->Q_DENOM->TabIndex = 53;
			this->Q_DENOM->Text = L"/";
			this->Q_DENOM->Click += gcnew System::EventHandler(this, &Main::Q_DENOM_Click);
			// 
			// Q_MUL
			// 
			this->Q_MUL->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_MUL->Location = System::Drawing::Point(448, 185);
			this->Q_MUL->Name = L"Q_MUL";
			this->Q_MUL->Size = System::Drawing::Size(52, 52);
			this->Q_MUL->TabIndex = 53;
			this->Q_MUL->Text = L"*";
			this->Q_MUL->Click += gcnew System::EventHandler(this, &Main::Q_MUL_Click);
			// 
			// Q_MINUS
			// 
			this->Q_MINUS->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_MINUS->Location = System::Drawing::Point(448, 127);
			this->Q_MINUS->Name = L"Q_MINUS";
			this->Q_MINUS->Size = System::Drawing::Size(52, 52);
			this->Q_MINUS->TabIndex = 34;
			this->Q_MINUS->Text = L"-";
			this->Q_MINUS->UseVisualStyleBackColor = true;
			this->Q_MINUS->Click += gcnew System::EventHandler(this, &Main::Q_MINUS_Click);
			// 
			// Q_PLUS
			// 
			this->Q_PLUS->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_PLUS->Location = System::Drawing::Point(448, 69);
			this->Q_PLUS->Name = L"Q_PLUS";
			this->Q_PLUS->Size = System::Drawing::Size(52, 52);
			this->Q_PLUS->TabIndex = 32;
			this->Q_PLUS->Text = L"+";
			this->Q_PLUS->UseVisualStyleBackColor = true;
			this->Q_PLUS->Click += gcnew System::EventHandler(this, &Main::Q_PLUS_Click);
			// 
			// Q_seven
			// 
			this->Q_seven->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->Q_seven->Location = System::Drawing::Point(506, 69);
			this->Q_seven->Name = L"Q_seven";
			this->Q_seven->Size = System::Drawing::Size(52, 52);
			this->Q_seven->TabIndex = 42;
			this->Q_seven->Text = L"7";
			this->Q_seven->UseVisualStyleBackColor = true;
			this->Q_seven->Click += gcnew System::EventHandler(this, &Main::Q_seven_Click);
			// 
			// Q_output
			// 
			this->Q_output->BackColor = System::Drawing::SystemColors::Window;
			this->Q_output->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->Q_output->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Q_output->Location = System::Drawing::Point(6, 6);
			this->Q_output->Name = L"Q_output";
			this->Q_output->ReadOnly = true;
			this->Q_output->Size = System::Drawing::Size(668, 44);
			this->Q_output->TabIndex = 31;
			this->Q_output->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// tabPage4
			// 
			this->tabPage4->Controls->Add(this->P_clr);
			this->tabPage4->Controls->Add(this->P_input_button);
			this->tabPage4->Controls->Add(this->P_output);
			this->tabPage4->Controls->Add(this->P_input);
			this->tabPage4->Location = System::Drawing::Point(4, 22);
			this->tabPage4->Name = L"tabPage4";
			this->tabPage4->Padding = System::Windows::Forms::Padding(3);
			this->tabPage4->Size = System::Drawing::Size(691, 416);
			this->tabPage4->TabIndex = 3;
			this->tabPage4->Text = L"Многочлены";
			this->tabPage4->UseVisualStyleBackColor = true;
			// 
			// P_input_button
			// 
			this->P_input_button->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->P_input_button->Location = System::Drawing::Point(546, 44);
			this->P_input_button->Name = L"P_input_button";
			this->P_input_button->Size = System::Drawing::Size(128, 40);
			this->P_input_button->TabIndex = 34;
			this->P_input_button->Text = L"Process";
			this->P_input_button->UseVisualStyleBackColor = true;
			this->P_input_button->Click += gcnew System::EventHandler(this, &Main::P_input_button_Click);
			// 
			// P_output
			// 
			this->P_output->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->P_output->Location = System::Drawing::Point(8, 90);
			this->P_output->Name = L"P_output";
			this->P_output->ReadOnly = true;
			this->P_output->Size = System::Drawing::Size(666, 110);
			this->P_output->TabIndex = 33;
			this->P_output->Text = L"";
			// 
			// P_input
			// 
			this->P_input->BackColor = System::Drawing::SystemColors::Window;
			this->P_input->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->P_input->CharacterCasing = System::Windows::Forms::CharacterCasing::Lower;
			this->P_input->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16));
			this->P_input->Location = System::Drawing::Point(6, 6);
			this->P_input->Name = L"P_input";
			this->P_input->Size = System::Drawing::Size(668, 32);
			this->P_input->TabIndex = 32;
			this->P_input->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// P_clr
			// 
			this->P_clr->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->P_clr->Location = System::Drawing::Point(413, 44);
			this->P_clr->Name = L"P_clr";
			this->P_clr->Size = System::Drawing::Size(127, 40);
			this->P_clr->TabIndex = 35;
			this->P_clr->Text = L"Clear";
			this->P_clr->UseVisualStyleBackColor = true;
			this->P_clr->Click += gcnew System::EventHandler(this, &Main::P_clr_Click);
			// 
			// Main
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(690, 461);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->MaximizeBox = false;
			this->Name = L"Main";
			this->Text = L"СКА";
			this->Load += gcnew System::EventHandler(this, &Main::Main_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			this->tabPage3->ResumeLayout(false);
			this->tabPage3->PerformLayout();
			this->tabPage4->ResumeLayout(false);
			this->tabPage4->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Main_Load(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void N_AddOne_Click(System::Object^  sender, System::EventArgs^  e) {
	N num;
	int i;
	for (i = NN_output->Text->Length - 1; i >= 0 && NN_output->Text[i] >= '0' && NN_output->Text[i] <= '9'; i--)
	{
		auto it = num.begin();
		num.insert(it, (int)NN_output->Text[i] - 48);
	}
	i++;
	num = ADD_1N_N(num);
	NN_output->Text = NN_output->Text->Remove(i);
	System::String^ str ("");
	for (int i = 0; i < num.size(); i++)
	{
		str = String::Concat(str, num[i].ToString());
	}
	NN_output->Text = NN_output->Text->Insert(i, str);
}
private: System::Void N_SubOne_Click(System::Object^  sender, System::EventArgs^  e) {
	N num, one;
	for (int i = 0; i < NN_output->Text->Length; i++)
	{
		num.push_back((int)NN_output->Text[i] - 48);
	}
	one.push_back(1);
	num = SUB_NN_N(num, one);
	NN_output->Text = "";
	for (int i = 0; i < num.size(); i++)
	{
		NN_output->Text = String::Concat(NN_output->Text, num[i].ToString());
	}
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = "";
}
private: System::Void N_GCF_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = "НОД: ";
}
private: System::Void N_LCM_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = "НОК: ";
}
private: System::Void N_Com_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = "Сравнить: ";
}
private: System::Void N_Comma_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = String::Concat(NN_output->Text, ", ");
}
private: System::Void DeleteLast_Click(System::Object^  sender, System::EventArgs^  e) {
	if (NN_output->Text->Length >= 1)
		NN_output->Text = NN_output->Text->Remove(NN_output->Text->Length - 1);
}
private: System::Void equal_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = Process_N(NN_output->Text);
}
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "1");
}
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "2");
}
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "3");
}
private: System::Void button13_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "4");
}
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "5");
}
private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "6");
}
private: System::Void button24_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "7");
}
private: System::Void button15_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "8");
}
private: System::Void button14_Click_1(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "9");
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, "0");
}
private: System::Void Z_COMMA_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = String::Concat(Z_output->Text, ", ");
}
private: System::Void Z_DLTLAST_Click(System::Object^  sender, System::EventArgs^  e) {
	if (Z_output->Text->Length >= 1)
		Z_output->Text = Z_output->Text->Remove(Z_output->Text->Length - 1);
}
private: System::Void Z_CLR_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = "";
}
private: System::Void Z_PLUS_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = Z_Process(Z_output->Text);
	Z_output->Text = String::Concat(Z_output->Text, " + ");
}
private: System::Void Z_MINUS_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = Z_Process(Z_output->Text);
	Z_output->Text = String::Concat(Z_output->Text, " - ");
}
private: System::Void Z_MUL_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = Z_Process(Z_output->Text);
	Z_output->Text = String::Concat(Z_output->Text, " * ");
}
private: System::Void Z_DIV_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = Z_Process(Z_output->Text);
	Z_output->Text = String::Concat(Z_output->Text, " div ");
}
private: System::Void Z_MOD_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = Z_Process(Z_output->Text);
	Z_output->Text = String::Concat(Z_output->Text, " mod ");
}
private: System::Void Z_NOD_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = "НОД: ";
}
private: System::Void Z_NOK_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = "НОК: ";
}
private: System::Void Z_COM_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = "Сравнить: ";
}
private: System::Void Z_ADDONE_Click(System::Object^  sender, System::EventArgs^  e) {
	Z num;
	num.pos = true;
	N one = { 1 };
	int i;
	for (i = Z_output->Text->Length - 1; i >= 0 && Z_output->Text[i] >= '0' && Z_output->Text[i] <= '9'; i--)
	{
		auto it = num.num.begin();
		num.num.insert(it, (int)Z_output->Text[i] - 48);
	}
	if (i >= 0 && Z_output->Text[i] == '-')
	{
		num.pos = false;
		i--;
	}
	i++;
	if (!NZER_N_B(num.num))
	{
		num.pos = true;
		num.num = ADD_1N_N(num.num);
	}
	else if (num.pos)
		num.num = ADD_1N_N(num.num);
	else
		num.num = SUB_NN_N(num.num, one);
	Z_output->Text = Z_output->Text->Remove(i);
	System::String^ str("");

	if (num.pos == false)
		str = String::Concat(str, "-");

	for (int i = 0; i < num.num.size(); i++)
	{
		str = String::Concat(str, num.num[i].ToString());
	}
	Z_output->Text = Z_output->Text->Insert(i, str);
}
private: System::Void Z_MINUSONE_Click(System::Object^  sender, System::EventArgs^  e) {
	Z num;
	num.pos = true;
	N one = { 1 };
	int i;
	for (i = Z_output->Text->Length - 1; i >= 0 && Z_output->Text[i] >= '0' && Z_output->Text[i] <= '9'; i--)
	{
		auto it = num.num.begin();
		num.num.insert(it, (int)Z_output->Text[i] - 48);
	}
	if (i >= 0 && Z_output->Text[i] == '-')
	{
		num.pos = false;
		i--;
	}
	i++;
	if (!NZER_N_B(num.num))
	{
		num.pos = false;
		num.num = ADD_1N_N(num.num);
	}
	else if (num.pos == false)
		num.num = ADD_1N_N(num.num);
	else
		num.num = SUB_NN_N(num.num, one);
	Z_output->Text = Z_output->Text->Remove(i);
	System::String^ str("");

	if (num.pos == false)
		str = String::Concat(str, "-");

	for (int i = 0; i < num.num.size(); i++)
	{
		str = String::Concat(str, num.num[i].ToString());
	}
	Z_output->Text = Z_output->Text->Insert(i, str);
}
private: System::Void Z_CHNG_Click(System::Object^  sender, System::EventArgs^  e) {
	Z num;
	int i;
	for (i = Z_output->Text->Length - 1; i >= 0 && Z_output->Text[i] >= '0' && Z_output->Text[i] <= '9'; i--)
	{
		auto it = num.num.begin();
		num.num.insert(it, (int)Z_output->Text[i] - 48);
	}
	if (i >= 0 && Z_output->Text[i] == '-')
	{
		num.pos = false;
		i--;
	}
	i++;
	num.pos = !num.pos;

	Z_output->Text = Z_output->Text->Remove(i);
	System::String^ str("");

	if (num.pos == false)
		str = String::Concat(str, "-");

	for (int i = 0; i < num.num.size(); i++)
	{
		str = String::Concat(str, num.num[i].ToString());
	}
	Z_output->Text = Z_output->Text->Insert(i, str);
}
private: System::Void Z_EQUAL_Click(System::Object^  sender, System::EventArgs^  e) {
	Z_output->Text = Z_Process(Z_output->Text);
}
private: System::Void Q_zero_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "0");
}
private: System::Void Q_one_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "1");
}
private: System::Void Q_two_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "2");
}
private: System::Void Q_three_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "3");
}
private: System::Void Q_four_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "4");
}
private: System::Void Q_five_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "5");
}
private: System::Void Q_six_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "6");
}
private: System::Void Q_seven_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "7");
}
private: System::Void Q_eight_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "8");
}
private: System::Void Q_nine_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, "9");
}
private: System::Void Q_CLR_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = "";
}
private: System::Void Q_DLTLST_Click(System::Object^  sender, System::EventArgs^  e) {
	if (Q_output->Text->Length >= 1)
		Q_output->Text = Q_output->Text->Remove(Q_output->Text->Length - 1);
}
private: System::Void Q_COM_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = "Сравнить: ";
}
private: System::Void Q_DENOM_Click(System::Object^  sender, System::EventArgs^  e) {
	if (Q_output->Text->Length == 0)
		Q_output->Text = String::Concat(Q_output->Text, "1");
	else if (Q_output->Text[Q_output->Text->Length - 1] == ' ')
		Q_output->Text = String::Concat(Q_output->Text, "1");
	Q_output->Text = String::Concat(Q_output->Text, "/");
}
private: System::Void Q_CHNG_Click(System::Object^  sender, System::EventArgs^  e) {
	Z num;
	int i;
	for (i = Q_output->Text->Length - 1; i >= 0 && (Q_output->Text[i] >= '0' && Q_output->Text[i] <= '9' || Q_output->Text[i] == '/'); i--)
	{
		auto it = num.num.begin();
		num.num.insert(it, (int)Q_output->Text[i] - 48);
	}
	if (i >= 0 && Q_output->Text[i] == '-')
	{
		num.pos = false;
		i--;
	}
	i++;
	num.pos = !num.pos;

	Q_output->Text = Q_output->Text->Remove(i);
	System::String^ str("");

	if (num.pos == false)
		str = String::Concat(str, "-");

	for (int i = 0; i < num.num.size(); i++)
	{
		if (num.num[i] == -1)
			str = String::Concat(str, "/");
		else
			str = String::Concat(str, num.num[i].ToString());
	}
	Q_output->Text = Q_output->Text->Insert(i, str);
}
private: System::Void Q_COMMA_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = String::Concat(Q_output->Text, ", ");
}
private: System::Void Q_PLUS_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = Q_Process(Q_output->Text);
	Q_output->Text = String::Concat(Q_output->Text, " + ");
}
private: System::Void Q_MINUS_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = Q_Process(Q_output->Text);
	Q_output->Text = String::Concat(Q_output->Text, " - ");
}
private: System::Void Q_MUL_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = Q_Process(Q_output->Text);
	Q_output->Text = String::Concat(Q_output->Text, " * ");
}
private: System::Void Q_EQUAL_Click(System::Object^  sender, System::EventArgs^  e) {
	Q_output->Text = Q_Process(Q_output->Text);
}

		 /*
		 *Функция, записывающая в поле вывода цифры (для натуральных чисел)
		 */
private: System::Void Write_Number_N(System::Object^  sender, System::EventArgs^  e) {
	Button^ butt = (Button^)sender;
	NN_output->Text = String::Concat(NN_output->Text, butt->Text);
}

		 /*
		 *Функция, записывающая в поле вывода операторы (для натуральных чисел)
		 */
private: System::Void WriteOpertor_N(System::Object^  sender, System::EventArgs^  e) {
	NN_output->Text = Process_N(NN_output->Text);
	Button^ butt = (Button^)sender;
	NN_output->Text = String::Concat(NN_output->Text, " " + butt->Text + " ");
}

		 /*
		 *Функция очистки поля ввода (для натуральных чисел)
		 */
private: System::Void clr_N_Click(System::Object^  sender, System::EventArgs^  e) {
	NN_output->ResetText();
}


		 /*
		 *Обработка введенного текста и вывод в output для полиномов
		 */
private: System::Void P_input_button_Click(System::Object^  sender, System::EventArgs^  e) {
	P_output->Clear();
	String^ str = P_input->Text;
	P result = Process_P(str);
	for (int i = 0; i < result.coef.size(); i++)
	{
		if (result.coef[i].nom.pos == true && i != 0)
			P_output->AppendText("+");
		P_output->AppendText(WriteNumber_Q(result.coef[i]));
		if (i > 0)
		{
			P_output->AppendText("x");
			if (i > 1)
			{
				P_output->SelectionFont = gcnew System::Drawing::Font(P_output->SelectionFont->FontFamily, 8, P_output->SelectionFont->Style);
				P_output->SelectionCharOffset = 5;
				P_output->SelectedText = i.ToString();
				P_output->AppendText(P_output->SelectedText);
				P_output->SelectionFont = gcnew System::Drawing::Font(P_output->SelectionFont->FontFamily, 12, P_output->SelectionFont->Style);;
				P_output->SelectionCharOffset = 0;
			}
		}
	}
	/*for (int i = 0; i < P_input->Text->Length; i++)
	{
		if ((P_input->Text[i] == '+' || P_input->Text[i] == '-' || P_input->Text[i] == '*') && P_output->SelectionCharOffset == 0)
		{
			if (P_output->SelectionCharOffset == 3 || P_output->SelectionCharOffset == -3)
				P_output->SelectionCharOffset = 0;
			P_output->AppendText(" " + P_input->Text[i].ToString() + " ");
		}
		else if (P_input->Text[i] == '^')
		{
			if (P_output->SelectionCharOffset == 3 || P_output->SelectionCharOffset == -3)
				P_output->SelectionCharOffset = 0;
			P_output->SelectionCharOffset += 5;
		}
		else if (P_input->Text[i] == ')')
		{
			if (P_output->SelectionCharOffset == 3 || P_output->SelectionCharOffset == -3)
				P_output->SelectionCharOffset = 0;

			if (P_output->SelectionCharOffset != 0)
				P_output->SelectionCharOffset -= 5;
			else
				P_output->AppendText(P_input->Text[i].ToString());
		}
		else if (P_input->Text[i] == '(')
		{
			if (P_output->SelectionCharOffset == 3 || P_output->SelectionCharOffset == -3)
				P_output->SelectionCharOffset = 0;

			if (P_input->Text[i - 1] != '^')
				P_output->AppendText(P_input->Text[i].ToString());
		}
		else if (P_input->Text[i] == '/')
		{ 
			if (P_input->Text[i - 1].IsDigit(P_input->Text[i - 1]))
			{
				System::String^ str = "";
				for (int j = i - 1; j >= 0 && P_input->Text[j].IsDigit(P_input->Text[j]); j--)
				{
					str = String::Concat(P_input->Text[j], str);
				}
				P_output->Text = P_output->Text->Remove(P_output->Text->Length - str->Length, str->Length);
				P_output->SelectionCharOffset += 3;
				for (int j = 0; j < str->Length; j++)
				{
					P_output->AppendText(str[j].ToString());
				}
				P_output->SelectionCharOffset = 0;
				P_output->AppendText("/");
				P_output->SelectionCharOffset -= 3;
			}
			else
			{
				if (P_output->SelectionCharOffset == 3 || P_output->SelectionCharOffset == -3)
					P_output->SelectionCharOffset = 0;

				P_output->AppendText(" " + P_input->Text[i].ToString() + " ");
			}
		}
		else
		{
			if ((P_output->SelectionCharOffset == 3 || P_output->SelectionCharOffset == -3) && !(P_input->Text[i].IsDigit(P_input->Text[i])))
				P_output->SelectionCharOffset = 0;
			P_output->AppendText(P_input->Text[i].ToString());
		}
	}*/
}
private: System::Void P_clr_Click(System::Object^  sender, System::EventArgs^  e) {
	P_input->ResetText();
}
};
}
