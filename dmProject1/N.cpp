﻿#include "N.hpp"


N RMVZ_N_N(N a)
{
	N result(0);

	int i = 0;
	for (; i < a.size() && a[i] == 0; i++);

	for (; i < a.size(); i++)
	{
		result.push_back(a[i]);
	}

	return result;
}


int COM_NN_D(N a, N b)
{
	a = RMVZ_N_N(a);
	b = RMVZ_N_N(b);

	if (a.size() > b.size())
		return 2;
	else if (a.size() < b.size())
		return 1;

	for (int i = 0; i < a.size() && i < b.size(); i++)
	{
		if (a[i] > b[i])
			return 2;
		else if (b[i] > a[i])
			return 1;
	}

	return 0;

}

bool NZER_N_B(N a)
{
	bool not_null = true;
	for (int i = 0; i < a.size(); i++)
	{
		if (a[i] != 0)
			return not_null;
	}
	return false;
}


N ADD_1N_N(N a)
{
	int p = 0;

	for (int i = a.size() - 1; i >= 0; --i)
	{
		if (a[i] == 9)
			a[i] = 0;
		else
		{
			a[i]++;
			p = 1;
			break;
		}
	}

	if (p != 1)
	{
		auto it = a.begin();
		a.insert(it, 1);
	}

	return a;
}


N ADD_NN_N(N a, N b)
{
	N c;
	int i, j, r(0);

	if (COM_NN_D(a, b) == 1)
		swap(a, b);

	for (i = b.size(); i < a.size(); i++)
	{
		auto it = b.begin();
		b.insert(it, 0);
	}

	for (j = a.size() - 1; j >= 0; j--)
	{
		auto it = c.begin();
		c.insert(it, (a[j] + b[j] + r) % 10);
		r = (a[j] + b[j] + r) / 10;
	}

	if (r != 0)
	{
		auto it = c.begin();
		c.insert(it, r);
		i++;
	}

	return c;
}


N SUB_NN_N(N a, N b)
{
	N result;

	int t = COM_NN_D(a, b);

	if (t == 2)
	{
		auto it = result.begin();
		for (int i = b.size() - 1, j = a.size() - 1; i >= 0 && j >= 0; i--, j--)
		{
			if (a[j] < b[i])
			{
				a[j] += 10;
				a[j - 1]--;
			}
			it = result.begin();
			result.insert(it, a[j] - b[i]);
		}
		for (int i = a.size() - b.size() - 1; i >= 0; i--)
		{
			it = result.begin();
			result.insert(it, a[i]);
		}
	}
	else if (t == 1)
		throw 5;
	else
		result.push_back(0);

	return result;
}


N MUL_ND_N(N a, int d)
{
	N result;
	int r = 0;
	auto it = result.begin();

	a = RMVZ_N_N(a);

	for (int i = a.size() - 1; i >= 0; i--)
	{
		it = result.begin();
		result.insert(it, a[i] * d % 10 + r);
		r = (a[i] * d + r) / 10;
	}

	if (r != 0)
	{
		it = result.begin();
		result.insert(it, r);
	}

	return result;
}


N MUL_Nk_N(N a, int k)
{
	N result;

	for (int i = 0; i < a.size(); i++)
		result.push_back(a[i]);

	for (int i = 0; i < k; i++)
		result.push_back(0);

	return result;
}


N MUL_NN_N(N a, N b)
{
	N result;
	result.push_back(0);

	int t = COM_NN_D(a, b);
	if (t == 1)
		swap(a, b);

	for (int i = 0; i < b.size(); i++)
	{
		result = ADD_NN_N(result, MUL_Nk_N(MUL_ND_N(a, b[i]), b.size() - i - 1));
	}

	return result;
}


N SUB_NDN_N(N a, N b, int d)
{
	N result, umng;
	int t = COM_NN_D(a, umng = MUL_ND_N(b, d));
	if (t == 2 && t == 0)
	{
		result = SUB_NN_N(a, umng);
	}
	else
		throw 9;

	return result;
}


N DIV_NN_Dk(N a, N b, int k)
{
	N result;
	result.push_back(0);

	int t = COM_NN_D(a, b);
	if (t == 1)
		return result;
	else if (t == 0)
		result[0] = 1;
	else
	{
		N c;
		b = RMVZ_N_N(b);
		a = RMVZ_N_N(a);
		for (int i = 0; i < b.size(); i++)
			c.push_back(a[i]);

		if (COM_NN_D(b, c) == 2)
			c.push_back(a[b.size()]);
		else if (COM_NN_D(b, c) == 0)
			result[0] = 1;

		int i = 0;
		while (COM_NN_D(c, b) != 1)
		{
			i++;
			c = SUB_NN_N(c, b);
		}
		result[0] = i;
	}

	if (COM_NN_D(MUL_NN_N(MUL_Nk_N(result, k), b), a) != 2)
		return MUL_Nk_N(result, k);
	else
		return MUL_Nk_N(result, k - 1);
}


N DIV_NN_N(N a, N b)
{
	if (!NZER_N_B(b))
		throw 11;

	N result;
	N subres;
	int i = 0;
	int k = a.size();
	while (COM_NN_D(b, a) != 2)
	{
		subres = DIV_NN_Dk(a, b, a.size() - b.size());
		result = ADD_NN_N(result, subres);
		N c = MUL_ND_N(b, subres[0]);

		if (COM_NN_D(a, MUL_Nk_N(c, a.size() - c.size())) != 1)
			c = MUL_Nk_N(c, a.size() - c.size());
		else
			c = MUL_Nk_N(c, a.size() - c.size() - 1);

		a = SUB_NN_N(a, c);
		a = RMVZ_N_N(a);
		/*if (!NZER_N_B(a))
			result = MUL_Nk_N(result, k - b.size() - i);*/
		i++;
	}

	/*if (!NZER_N_B(a) && i != k - b.size() + 1)
		result = MUL_Nk_N(result, k - b.size() - i + 1);*/

	return result;
}


N MOD_NN_N(N a, N b)
{
	if (!NZER_N_B(b))
		throw 12;
	N mod;
	
	mod = SUB_NN_N(a, MUL_NN_N(b, DIV_NN_N(a, b)));

	return mod;	
}


N GCF_NN_N(N a, N b)
{
	N r, GCF;

	if (!NZER_N_B(b))
		return a;
	r = b;
	GCF = b;

	while (NZER_N_B(r))
	{
		r = MOD_NN_N(a, b);
		if (NZER_N_B(r))
		{
			GCF = r;
			a = b;
			b = r;
		}
	}

	GCF = b;

	return GCF;
}


N LCM_NN_N(N a, N b)
{
	N LCM;

	LCM = DIV_NN_N(MUL_NN_N(a, b), GCF_NN_N(a, b));

	return LCM;
}