﻿#include "Z.hpp"

N ABS_Z_N(Z a)
{
	return a.num;
}


int POZ_Z_D(Z a)
{
	if (!NZER_N_B(a.num))
		return 0;
	else
		return (int)a.pos;
}


Z MUL_ZM_Z(Z a)
{
	a.pos = !a.pos;

	return a;
}


Z TRANS_N_Z(N a)
{
	Z b;
	b.pos = true;
	b.num = a;

	return b;
}


N TRANS_Z_N(Z a)
{
	if (a.pos = false)
		throw 5;

	N b;
	b = a.num;

	return b;
}


Z ADD_ZZ_Z(Z a, Z b)
{
	Z result;
	if (a.pos == b.pos)
	{
		result.pos = a.pos;
		result.num = ADD_NN_N(a.num, b.num);
	}
	else
	{
		int t = COM_NN_D(a.num, b.num);
		if (t == 2)
		{
			if (a.pos == true)
				result.pos = true;
			else
				result.pos = false;

			result.num = SUB_NN_N(a.num, b.num);
		}
		else if (t == 0)
		{
			result.pos = true;
			result.num.push_back(0);
		}
		else
		{
			if (b.pos == true)
				result.pos = true;
			else
				result.pos = false;

			result.num = SUB_NN_N(b.num, a.num);
		}
	}

	return result;
}


Z SUB_ZZ_Z(Z a, Z b)
{
	Z result;

	b.pos = !b.pos;

	result = ADD_ZZ_Z(a, b);

	return result;
}


Z MUL_ZZ_Z(Z a, Z b)
{
	Z result;

	result.pos = !(a.pos ^ b.pos);

	result.num = MUL_NN_N(a.num, b.num);

	return result;
}


Z DIV_ZZ_Z(Z a, Z b)
{
	Z result;

	if (NZER_N_B(b.num))
	{
		result.pos = !(a.pos ^ b.pos);

		result.num = DIV_NN_N(a.num, b.num);
	}
	else
		throw 9;

	return result;
}


Z MOD_ZZ_Z(Z a, Z b)
{
	Z result;

	if (NZER_N_B(b.num))
	{
		result.pos = !(a.pos ^ b.pos);

		result.num = MOD_NN_N(a.num, b.num);
	}
	else
		throw 10;

	if (result.pos == false)
	{
		result.pos = true;
		result.num = SUB_NN_N(b.num, result.num);
	}

	return result;
}