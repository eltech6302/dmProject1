#ifndef PROC
#define PROC
#include <vector>
#include "P.hpp"
using namespace System;
using namespace std;

/**
*��������� ������� ������
*/

/**
* ���������� ������������ �����.
* @param {String^} str ������� ������
* @return {N} num ����������� �����
* @author ������� ������
*/
N ReadNumber_N(System::String^);


/**
* ������ ������������ �����.
* @param {N} num �����
* @return {System::string^} str ����� � ���� ������
* @author ������� ������
*/
System::String^ WriteNumber_N(N);


/**
* ��������� �������� � ������������ �������.
* @param {String^} str ������� ������
* @return {String^} str ������ � �����������
* @author ������� ������
*/
System::String^ Process_N(System::String^);


/**
* ���������� ������ �����.
* @param {String^} str ������� ������
* @return {Z} num ����� �����
* @author ������� ������
*/
Z ReadNumber_Z(System::String^);


/**
* ������ ������ �����.
* @param {Z} num ����� �����
* @return {String^} str ����� � ���� ������
* @author ������� ������
*/
System::String^ WriteNumber_Z(Z);


/**
* ��������� �������� � ������ �������.
* @param {String^} str ������� ������
* @return {String^} str ������ � �����������
* @author ������� ������
*/
System::String^ Z_Process(System::String^);


/**
* ���������� ������������� �����.
* @param {String^} str ������� ������
* @return {Q} num ������������ �����
* @author ������� ������
*/
Q ReadNumber_Q(System::String^);


/**
* ������ ������������� �����.
* @param {Q} num ������������ �����
* @return {String^} str ������ � �����������
* @author ������� ������
*/
Q ReadNumber_Q(System::String^);


/**
* ������ ������������� �����.
* @param {Q} num ������������ ����� �����
* @return {String^} str ����� � ���� ������
* @author ������� ������
*/
System::String^ WriteNumber_Q(Q);


/**
* ��������� �������� � ������������� �������.
* @param {String^} str ������� ������
* @return {String^} str ������ � �����������
* @author ������� ������
*/
System::String^ Q_Process(System::String^);

P ReadPolinom(System::String^);
P Process_P(System::String^);
void Output_P(P);

#endif