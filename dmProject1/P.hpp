﻿#ifndef P_HPP
#define P_HPP

#include <vector>
#include "Q.hpp"

/**
* Полиномы (многочлены)
*/
typedef struct P
{
	std::vector <Q> coef;
	N deg;
};

/**
* Сложение многочленов
* @param {P} a Первый полином
* @param {P} b Второй полином
* @return {P} a + b
* @author Матвей Терентьев
*/
P ADD_PP_P(P a, P b);

/**
* Вычитание многочленов
* @param {P} a Первый полином
* @param {P} b Второй полином
* @return {P} a - b
* @author Матвей Терентьев
*/
P SUB_PP_P(P a, P b);

/**
* Умножение многочлена на рациональное число
* @param {P} a Полином
* @param {Q} k Множитель
* @return {P} a * k
* @author Александр Поздеев +
*/
P MUL_PQ_P(P a, Q k);

/**
* Умножение многочлена на x^k
* @param {P} a Полином
* @param {N} k Степень x
* @return {P} a * x^k
* @author Александр Поздеев +
*/
P MUL_Pxk_P(P a, N k);

/**
* Старший коэффициент многочлена
* @param {P} a Полином
* @return {Q} Коэффициент при x в наибольшей степени
* @author Алексей Кузнецов
*/
Q LED_P_Q(P a);

/**
* Степень многочлена
* @param {P} a Полином
* @return {N} Степень многочлена
* @author Алексей Кузнецов
*/
N DEG_P_N(P a);

/**
* Вынесение из многочлена НОК знаменателей коэффициентов и НОД числителей
* @param {P} a Полином
* @return {Q} НОД(числителей) / НОК(знаменателей)
* @author Артём Терентьев
*/
Q FAC_P_Q(P a);

/**
* Умножение многочленов
* @param {P} a Полином
* @param {P} b Полином
* @return {P} a * b
* @author Артём Терентьев
*/
P MUL_PP_P(P a, P b);

/**
* Частное от деления многочлена на многочлен при делении с остатком
* @param {P} a Полином
* @param {P} b Полином
* @return {P} a div b
* @author Кирилл Логвинов
*/
P DIV_PP_P(P a, P b);

/**
* Остаток от деления многочлена на многочлен при делении с остатком
* @param {P} a Полином
* @param {P} b Полином
* @return {P} a mod b
* @author Кирилл Логвинов
*/
P MOD_PP_P(P a, P b);

/**
* НОД многочленов
* @param {P} a Полином
* @param {P} b Полином
* @return {P} НОД(a, b)
* @author Илья Барбарич +
*/
P GCF_PP_P(P a, P b);

/**
* Производная многочлена
* @param {P} a Исходный полином
* @return {P} a' = da/dx (производная, в общем)
* @author Alina Mjk
*/
P DER_P_P(P a);

/**
* Преобразование многочлена — кратные корни в простые
* @param {P} a Исходный полином
* @return {P} Преобразованный полином
* @author Александр Кащеев
*/
P NMR_P_P(P a);

#endif //P_HPP