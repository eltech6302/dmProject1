#include "P.hpp"

P ADD_PP_P(P p1, P p2)
{
	if (p1.coef.size() < p2.coef.size())
	{
		swap(p1.coef, p2.coef);
		N tmp = p1.deg;
		p1.deg = p2.deg;
		p2.deg = tmp;
	}

	for (int i = 0; i < p2.coef.size(); i++)
	{
		p1.coef[i] = ADD_QQ_Q(p1.coef[i], p2.coef[i]);
	}

	return p1;
}


P SUB_PP_P(P p1, P p2)
{
	if (p1.coef.size() < p2.coef.size())
	{
		swap(p1.coef, p2.coef);
		N tmp = p1.deg;
		p1.deg = p2.deg;
		p2.deg = tmp;
	}

	for (int i = 0; i < p1.coef.size(); i++)
	{
		p1.coef[i] = SUB_QQ_Q(p1.coef[i], p2.coef[i]);
	}

	return p1;
}

//������� ��������� ���������� �� ������������ �����
//������� ������: Pol - ���������, ���������� �������, M - �����, �� ������� ���� ��������
//� ����� for ������ ������� ������� �������������, ������� � 0, �������� �� �����,
//��� ����� �������� ���� ����������� � ������� ����� � �������,
//����������� ��������� ������������ �����

P MUL_PQ_P(P Pol, Q M)
{
	for (int i = 0; i < Pol.coef.size(); i++)
	{
		MUL_QQ_Q(Pol.coef[i], M);
	}

	return Pol;
}



//������� ��������� ����������� �� � � ������� k
//������� ������: Pol - ���������, ���������� �������, k - �������
//� ������ ������� ��� ���������, � ������� ������� ����� ����� ����� �������� ���� � ������
//� ����� for ��������� ���� ������ � ����� ������� �������������,
//����� ���������� � ��� ����
//��� �� ������ ��� ����������� ������� ������� �� 1
//
P MUL_Pxk_P(P Pol, N k)
{
	Z nul;
	nul.num.push_back(0);

	Z one;
	one.num.push_back(1);

	Q n = { nul, one };

	N i = {0};

	for (int j = 0; j < Pol.coef.size(); j++)
	{
		i = ADD_1N_N(i);
	}

	k = ADD_NN_N(i, k);

	while (COM_NN_D(i, k) == 1)
	{
		auto it = Pol.coef.begin();
		Pol.coef.insert(it, n);
		i = ADD_1N_N(i);
	}

	return Pol;
}


Q LED_P_Q(P a)
{
	return a.coef[a.coef.size()];
}

N DEG_P_N(P a)
{
	return a.deg;
}



Q FAC_P_Q(P a) {
	N nod = (ABS_Z_N(a.coef[a.coef.size()].nom));  //���, ��������� ��������� - ����� ��������� ������� �����������
	N nok = TRANS_Z_N(a.coef[a.coef.size()].denom);           //���, ��������� ��������� - ����� ����������� ������� �����������
	Q res;
										   //�������� �� ������ ����������
	auto it = --(a.coef.begin());


	while (it != (a.coef.begin() - 1)) {                   //�������� �� ��, ��� �� ��������� ��� �����������
		nod = GCF_NN_N(nod, ABS_Z_N((*it).nom));  //������� ��� ���������� ���� ������������
		nok = LCM_NN_N(nok, TRANS_Z_N((*it).denom));         //������� ��� ������������ ���� ������������
		it--;                                     //������� ��������
	}

	it = a.coef.end(); //�������� �� ������ ����������

	while (it != (a.coef.begin() - 1)) {                         //�������� �� ��, ��� �� ��������� ��� �����������
		bool pos1 = (*it).nom.pos;                        //��������� ���� ��������� �����������
		(*it).nom.pos = true;                             //����� ������ ��������� �����������
		(*it).nom = DIV_ZZ_Z((*it).nom, TRANS_N_Z(nod));  //����� ��������� ����������� �� ��� ���������� ������������
		(*it).nom.pos = pos1;                             //���������� �������� ���� ��������� �����������
		(*it).denom = TRANS_N_Z(DIV_NN_N(TRANS_Z_N((*it).denom), nok));         //����� ����������� ����������� �� ��� ������������ ������������
		it--;
	}

	res.nom = TRANS_N_Z(nod);
	res.denom = TRANS_N_Z(nok);

	return res;  //���������� ���������� ���������
}

P MUL_PP_P(P a, P b)
{
	N zero; zero.push_back(0);  //���������� ����
	N one; one.push_back(1);   //���������� �������

	P sum; sum.coef.push_back(TRANS_Z_Q(TRANS_N_Z(zero)));  //������� ������-������� ��������� �����
	N deg; deg = zero;                                                //��������������� �����, ������� � ��� �� ����� ������� ������ ���������
	

	while ((b.coef.size()) != 0)
	{                                          //�������� �� ��, ��� ������ ��������� �� ����
		sum = ADD_PP_P(sum, MUL_PQ_P(MUL_Pxk_P(a, deg), b.coef.front()));  //���������� � ����� ��������� A ��������� �� ��������� �������� ���������� B
																				
		deg = ADD_1N_N(deg);					//������� �������� �� ������� ��� ��������
		b.coef.erase(b.coef.begin(), b.coef.begin() + 1);

	}

	return sum;  //���������� ���������� ���������
}


P GCF_PP_P(P a, P b)
{
	P r, NOD;
	if (DEG_P_N(a) >= DEG_P_N(b))         //��������� ������� �������
		r = MOD_PP_P(a, b);
	else if (DEG_P_N(a) < DEG_P_N(b))
	{
		r = MOD_PP_P(b, a);
		b = a;
	}

	while (r.coef[0].nom.num[0] != 0);
	{
		a = b;
		b = r;
		r = MOD_PP_P(a, b);
	}

	if (r.coef[0].nom.num[0] == 0)
		NOD = b;

	return NOD;
}


P DIV_PP_P(P a, P b)
{
	P divAB, var;                     // divAV - ��������� ������ �������; var - ������������� ����������
	Q chastnoe;                       // ���������� ��� �������� �������� ������������� ��� ������� ��������
	N degA = DEG_P_N(a);              // degA - ������� ������� � �������� �
	N degB = DEG_P_N(b);              // degB - ������� ������� � �������� b 
	N raznica = SUB_NN_N(degA, degB); //�������� ������� � � b
	if (b.coef.size() > a.coef.size())              // ���� ������ b ������ ������� a
		divAB = SUB_PP_P(a, a);        // ����������� ������ ������� ����� 0=(�-�)
	else
	{
		while (b.coef.size() <= a.coef.size())     // ���� ������� b �� ������ ������� �
		{
			chastnoe = DIV_QQ_Q(a.coef[0], b.coef[0]);         // ������� ������� ������������� ��� ������� ��������
			var = MUL_Pxk_P(b, raznica);             // ���������� �� ��������� ��������� ������� b ����������� �� x^(�������� ������� � � b)
			var = MUL_PQ_P(var, chastnoe);           // ��������� �� ������� ������������� ��� ������� ��������
			a = SUB_PP_P(a, var);                    // �������� �� a ���������� var, ���������� � �
			degA = DEG_P_N(a);                       // ��������� �������� ������� � 
			raznica = SUB_NN_N(degA, degB);          // ������� �������� ������� � � b
			divAB.coef.push_back(chastnoe);               // � ����� ��������-��������� ���������� �����������-������� ������������� ��� ������� �������� � � � b
		}
	}
	return divAB;   // ���������� �������-������� �� ������� a �� b
}


P MOD_PP_P(P a, P b)
{
	P var = DIV_PP_P(a, b); // � ���������� ���������� ������� �� ������� � �� b
	var = MUL_PP_P(b, var); // �������� �� �������
	return SUB_PP_P(a, var); // ���������� ������� � � var - ������� �� �������
}


P DER_P_P(P a)
{
	N i = { 0 };
	N size = DEG_P_N(a);
	int j = 0;
	while (COM_NN_D(i, size) == 1)
	{
		a.coef[j] = MUL_QQ_Q(a.coef[j], TRANS_Z_Q(TRANS_N_Z(SUB_NN_N(size, i))));
		j++;
		i = ADD_1N_N(i);
	}

	return a;
}